# -*- coding: utf-8 -*-
"""
Created on Mon Mar 12 10:45:59 2018

@author: bexter
"""

import configparser
#import tkinter
import shutil
import os
import time
#from PIL import Image, ImageTk
import ast

import var

class Maze:
    
    def __init__(self, grid_x, grid_y, mazefile="InputMaze.maze"):
        #maze parts
        self.tracklines = []
        self.sensorWallNoL = []
        self.sensorWallInactive = []
        self.falseWallNoL = []
        self.rewardZone = []
        self.mazeWall = []
        self.sensor_Pos = []
        self.sensorWall = []
        self.falseWall = []
        self.data = configparser.ConfigParser()
        self.data.read(mazefile)

        self.grid_xpos = int(grid_x)
        self.grid_ypos = int(grid_y)
        mazeWall_xpos = 0
        mazeWall_ypos = 0

        self.waypoint = [] #stay here?
        
    def get(self, window_name, field_name):
        return self.data.get(window_name, field_name)
    
    def write(self, filename, name, valueDict):
        self.data[name] = valueDict
        with open(filename, 'w+') as datafile:
            self.data.write(datafile)
            
    def clearMaze(self, canvas):  
        
        for entry in self.rewardZone:
            canvas.delete(entry)
    
        for Wall in self.mazeWall:
            canvas.delete(Wall)
    
        for Sensor in self.sensorWall:
            canvas.delete(Sensor)
    
        for Wall in self.falseWall:
            canvas.delete(Wall)
    
        for SensorL in self.sensorWallNoL:
            canvas.delete(SensorL)
    
        for WallL in self.falseWallNoL:
            canvas.delete(WallL)

        canvas.delete(self.startPosMarker)
        canvas.delete(self.mouseMarker)
        canvas.delete(self.teleportMarker)
        self.sensorWallNoL = []
        self.falseWallNoL = []
        self.rewardZone = []
        self.mazeWall = []
        self.sensor_Pos = []
        self.sensorWall = []
        self.falseWall = []
        
    def load_maze(self, GUI, GUIRules, config):
        import tkinter
        LoadMazeFile = tkinter.filedialog.askopenfilename()
        if LoadMazeFile != '':
            self.clearMaze(GUI.Canvas)
            shutil.copyfile(LoadMazeFile, "InputMaze.maze")
            self.data = configparser.ConfigParser()
            self.data.read("InputMaze.maze")
            # maze title
            
            mazeName = str(os.path.basename(LoadMazeFile)) 
            
            GUI.LmTitle.config(
                text=("Maze: " + mazeName))
            
            config.set_value(
                'Files', 'MazeFile', mazeName)

            #create the new maze
            self.buildMaze(GUI, config)

            #Close rule window and restart it with new maze
            if GUI.isOpen(GUIRules):
                GUIRules.window.destroy()
                GUIRules.openWindow(self)
                
            print("save ")    
            #copy textures into the _temp folder for blender
            shutil.copyfile("Data/wall_textures/maze_textures/" + 
                            mazeName + "/ceiling.png",
                            "Data/wall_textures/maze_textures/_temp/ceiling.png")
            
            shutil.copyfile("Data/wall_textures/maze_textures/" + 
                            mazeName + "/floor.png",
                            "Data/wall_textures/maze_textures/_temp/floor.png")
            
            shutil.copyfile("Data/wall_textures/maze_textures/" + 
                            mazeName + "/wall.png",
                            "Data/wall_textures/maze_textures/_temp/wall.png")
            
    def buildMaze(self, GUI, config):
        import tkinter
        from PIL import Image, ImageTk
        for wallNo in self.data['Walls']:  
            # build Walls
            _wall = ast.literal_eval(self.data.get('Walls', wallNo))
            if self.get('settings','endless') == 'True' and \
                    ( int(wallNo)==0 or int(wallNo)==2 ):
                sensor_tele = ast.literal_eval(self.data.get('Sensors','0'))
                self.mazeWall.append(GUI.Canvas.create_line(
                    float(_wall[0]) + self.grid_xpos,
                    float(_wall[1]) + self.grid_ypos,
                    float(_wall[2])+ self.grid_xpos,
                    float(sensor_tele[3])+ self.grid_ypos,
                    width=2, fill='#7D26CD'))
            else:
                self.mazeWall.append(GUI.Canvas.create_line(
                    float(_wall[0]) + self.grid_xpos,
                    float(_wall[1]) + self.grid_ypos,
                    float(_wall[2]) + self.grid_xpos,
                    float(_wall[3]) + self.grid_ypos,
                    width=2, fill='#7D26CD'))
        
        for rewNo in self.data['Rewards']:
            _entry = ast.literal_eval(self.data.get('Rewards',rewNo))
            self.rewardZone.append(GUI.Canvas.create_oval(
                _entry[0] - 5 + self.grid_xpos,
                _entry[1] - 5 + self.grid_ypos,
                _entry[0] + 5 + self.grid_xpos,
                _entry[1] + 5 + self.grid_ypos,
                fill='#1C86EE'))

        _entry = ast.literal_eval(self.data.get('Positions','start'))
        self.startPosMarker = GUI.Canvas.create_oval(
            _entry[0] - 5 + self.grid_xpos,
            _entry[1] - 5 + self.grid_ypos,
            _entry[0] + 5 + self.grid_xpos,
            _entry[1] + 5 + self.grid_ypos,
            fill='#49E20E')
        
        # create mouse marker
        self.mouseMarker = GUI.Canvas.create_oval(
            _entry[0] - 5 + self.grid_xpos,
            _entry[1] - 5 + self.grid_ypos,
            _entry[0] + 5 + self.grid_xpos,
            _entry[1] + 5 + self.grid_ypos, 
            fill='#37FDFC')
        
        var.mouse_x = round(_entry[0], 2) ###---> mouse position only set here
        var.mouse_y = round(_entry[1], 2) # was commented out
        
        
        #teleport position
        _entry = ast.literal_eval(self.data.get('Positions','teleport'))
        if _entry[1] < 700:
            self.teleportMarker = GUI.Canvas.create_oval(
                _entry[0] - 5 + self.grid_xpos,
                _entry[1] - 5 + self.grid_ypos,
                _entry[0] + 5 + self.grid_xpos,
                _entry[1] + 5 + self.grid_ypos, 
                fill='#FF6600')
        else:
            self.teleportMarker = GUI.Canvas.create_oval(
                -10,-5,-10,-5, fill='#FF6600')
            
        if self.data.has_section('Sensors'):       
            for sensorNo in self.data['Sensors']:
                _entry = ast.literal_eval(self.data.get('Sensors',sensorNo))
                self.sensorWall.append(GUI.Canvas.create_line(
                    _entry[0] + self.grid_xpos,
                    _entry[1] + self.grid_ypos,
                    _entry[2] + self.grid_xpos,
                    _entry[3] + self.grid_ypos,
                    width=2, tags='currentSensor', fill="#FF4500", dash=3))
                self.sensorWallInactive.append(0)

                self.sensorWallNoL.append(GUI.Canvas.create_text(
                    _entry[0] + self.grid_xpos - 5,
                    _entry[1] + self.grid_ypos,
                    text=str(sensorNo)))
        if self.data.has_section('False Walls'):            
            for falwallNo in self.data['False Walls']:
                _entry = ast.literal_eval(self.data.get('False Walls',falwallNo))
                self.falseWall.append(GUI.Canvas.create_line(
                    _entry[0] + self.grid_xpos,
                    _entry[1] + self.grid_ypos,
                    _entry[2] + self.grid_xpos,
                    _entry[3] + self.grid_ypos,
                    width=2, tags='currentSensor', fill="#668014", dash=3))

                self.falseWallNoL.append(GUI.Canvas.create_text(
                    _entry[0] + self.grid_xpos - 5,
                    _entry[1] + self.grid_ypos,
                    text=str(falwallNo)))

        # create cue
        self.cueMarker = GUI.Canvas.create_rectangle(
            -100, -90, 0, 0, width=1, fill='#EE2C2C')



        ####GO TO GUI or somewhere!!!!
        if self.get('settings','endless') == 'True':     
            #exp.useRules.set(False) #?
            #exp.newTrialTeleport.set(True) #?
            CueTex = "../Data/wall_textures/%s.jpg" % \
                config.get('Files', 'MazeFile')
            if CueTex != '':
                shutil.copyfile(CueTex, "../../Data/wall_textures/WallLeft.jpg")
                image = Image.open("../Data/wall_textures/WallLeft.jpg")
                width, height = image.size
                config.set_value(
                    'config.ini','conf', 'imgfactl', str(int(width/height)))

                image.thumbnail(
                    float(GUI.get('define maze walls','width'))/2-20, 
                    40, Image.ANTIALIAS)

                shutil.copyfile(CueTex, "../Data/wall_textures/WallRight.jpg")

                WallPicMiniRThump = ImageTk.PhotoImage(image)
                WallPicMinivR = tkinter.Label(GUI.Canvas, image=WallPicMiniRThump)
                WallPicMinivR.place(
                    x=mazeWall_xpos \
                        + float(GUI.get('define maze walls','width')) / 2 + 5,
                    y=mazeWall_ypos + 110)
                
                
                WallPicMiniLThump = ImageTk.PhotoImage(image)
                WallPicMiniL = tkinter.Label(GUI.Canvas, image=WallPicMiniLThump)
                WallPicMiniL.place(
                    x=mazeWall_xpos \
                        + float(GUI.get('define maze walls','width')) / 2 + 5,
                    y=mazeWall_ypos + 65)

            GUI.LEndlessCorridor.config(text="Endless Corridor:  " + str(True))   
            

        # search for existing textures:
        CueTexturePhoto = [[] for i in range(10)]
        for i in range(10):
            if os.path.isfile("Texture_%d.bmp" % i):
                # CueNo = CueNo+1
                with open("Texture_%d.bmp" % i, 'rb') as image_file:
                    with Image.open(image_file) as image:
                        image.thumbnail((34, 34), Image.ANTIALIAS)
                        CueTexturePhoto[i] = ImageTk.PhotoImage(image)
                        GUI.cuePic[i] = tkinter.Label(image=CueTexturePhoto[i])
                        GUI.cuePic[i].place(
                            x=GUI.cue_dict['pic_x_pos'][i] \
                                + (GUI.cue_dict['width']-38) / 2,
                            y=GUI.cue_dict['pic_y_pos'][i])
        
    def pos_mouse(self, xPos, yPos, canvas):
        canvas.coords(
          self.mouseMarker,
          xPos - 5 + self.grid_xpos, 
          yPos - 5 + self.grid_ypos,
          xPos + 5 + self.grid_xpos, 
          yPos + 5 + self.grid_ypos)    
        
    def createGrid(self, GUI):
        # vertical lines at an interval of "line_distance" pixel
        for x in range(
                int(GUI.get('maze_grid','grid_res')), 
                int(GUI.get('maze_grid','width')), 
                int(GUI.get('maze_grid','grid_res'))):
            GUI.Canvas.create_line(
                self.grid_xpos + x, 
                self.grid_ypos, 
                self.grid_xpos + x, 
                self.grid_ypos + float(GUI.get('maze_grid','height')),
                fill="#476042")
        # horizontal lines at an interval of "line_distance" pixel
        for y in range(
                int(GUI.get('maze_grid','grid_res')), 
                int(GUI.get('maze_grid','height')), 
                int(GUI.get('maze_grid','grid_res'))):
            GUI.Canvas.create_line(
                self.grid_xpos, 
                self.grid_ypos+y,
                self.grid_xpos+float(GUI.get('maze_grid','width')), 
                self.grid_ypos+y, 
                fill="#476042")


    def update_sensors(self, canvas):
        for n, i in enumerate(self.sensorWallInactive):
            if i > 0:
                if time.time() >= i:
                    self.sensorWallInactive[n] = 0
                    canvas.itemconfig(self.sensorWall[n], fill="#FF4500")


    def initialization(self, GUI, config):
      print('Building Maze...')
      try:
          self.buildMaze(GUI, config)
      except:
          print("ERROR in setting up maze, Reset to standard")
          shutil.copyfile("_stMaze.maze", "InputMaze.maze")
          self.buildMaze(GUI, config)
      print('done')


    def position_cue(self, canvas, x, y):
        """place the marker for cues at the cue position (x,y)"""
        canvas.coords(
            self.cueMarker,
            x - 3 + self.grid_xpos,
            y - 3 + self.grid_ypos, 
            x + 3 + self.grid_xpos,
            y + 3 + self.grid_ypos)
