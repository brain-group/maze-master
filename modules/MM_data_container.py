# -*- coding: utf-8 -*-

import os
import csv
import datetime
import tkinter as tk
from tkinter import messagebox as mbox

import MM_trial

'''
parameter before and here in general: 
GUI.eFileBaseName.get() = file_name
GUI.eDataDir.get() = save_file_path
window.eSILogDir.get() = log_path
GUI.saveFilesTo.get() = save_to
'''

class DataContainer:
    
    def __init__(self, data_type=None):
        """ 
        Define container (dict) for trial data 
        (and the column order to save them)
        argument: data_type in {trialData, trialMetadata, blockMetaData}
        """

        self.data_type = data_type

        if data_type == 'trialData':
            self.data = {
            'time': [],
            'xpos': [],
            'ypos': [],
            'steps': [],
            'actual_position': [],
            'sensor_barrier_ID': [],
            'reward_ID': [],
            'cue_left': [],
            'cue_right': [],
            'lick': [],
            'reward': []}
            self._file_name_add = '_tr-%d' % MM_trial.Trials.trial_no
            self.append = True
        elif data_type == 'trialMetadata':
            self.data = {
            'trial_ID': [],
            'trial_start': [],
            'trial_stop': [],
            'trial_dur': [],
            'Target_side': [],
            'target': [],
            'distractor': [],
            'modality': [],
            'cue_duration':[],
            'Answer_side': [],
            'Replay_file': [],
            'sequence_file': [],
            'commented': []}
            self._file_name_add = '_mdata-trials'
            self.append = True
        elif data_type == 'blockMetadata':
            self.data = {
            "Date": [datetime.date.today().isoformat()],
            "Weekday": [datetime.date.today().strftime('%A')],
            "ExperimentType": [None],
            "TaskType": [None],
            "MouseID": ['n.d.'],
            "BlockID": [-1],
            "DataDirectory": ['n.d.'],
            "FileBaseName": ['n.d.'],
            "SILogDirectory": ['n.d.'],
            "StartTime": ["%H:%M:%S.%f"],
            "EndTime": ["%H:%M:%S.%f"],
            "TotalTrialNo": [0]}
            self._file_name_add = '_mdata'
            self.append = False
        else:
            self.data = {}
            self._file_name_add = ''
            self.append = False


    def save_temp_config(GUI):  # maybe save somewhere else
        '''
        creates a config file with the data paths
        '''

        save_to = []
        if GUI.saveFilesTo.get() == 'Network':
            save_to += ["", r"\NASKAMPA", "Data"]
        elif GUI.saveFilesTo.get() == 'LocalComp':
            save_to += ["", "Data"]
        else:
            return ""
    
        if GUI.expType.get() != "undefined":
            save_to += [GUI.expType.get()]
    
        save_to += ["temp_config.csv"]
    
        fd = open("\\".join(save_to), 'w')
        # define write mode of the csv
        config_file = csv.writer(
            fd, dialect='excel', quotechar='"', quoting=csv.QUOTE_ALL,
            lineterminator='\n', delimiter=';')
        # write data into file
        for inp in [["data_dir", GUI.eDataDir.get()],
                    ["file_basename", GUI.eFileBaseName.get()],
                    ["silog_dir", GUI.eSILogDir.get()],
                    ["current_trialid", MM_trial.Trials.trial_no]]:  
            config_file.writerow(inp)
        # close the file object
        fd.close()
    
    def save_data(self, data, save_to, save_file_path, file_name, 
                  timeDisplay, time, GUI): 
        '''
        in general: saves trial data in a .csv file (additional call 
        of functions) and reset the trial-container
        data should contain: data = [trialData, trialMetaData, blockMetaData]
        '''
        
        if not save_to == "Nowhere":
    
            if len(data[0].data['time']) > 0:

                # trial data: save to file and reset
                data[0].data_to_csv(save_to, save_file_path, file_name)
                data[0].__init__(data[0].data_type)

                # trial metadata: update
                data[1].add(trial_stop=time, trial_dur=round(
                        timeDisplay*1000, 0))
                if len(data[1].data["commented"]) == 0:
                    data[1].data["commented"] = ["no"]
                    
                data[1].add(cue_duration=GUI.eCueTime.get())    

                #trial metadata: save to file and reset 
                data[1].data_to_csv(save_to, save_file_path, file_name)
                data[1].__init__(data[1].data_type)


                # block metadata: update data and file
                data[2].update(EndTime = time, 
                    TotalTrialNo = MM_trial.Trials.trial_no)
                data[2].replace_data_in_file(
                    save_to, save_file_path, file_name, EndTime=[time],
                    TotalTrialNo=[MM_trial.Trials.trial_no+1])
         
        else:
            print("This is a test run without saving data or metadata.")

    def write_control_trial_data(save_file_path, file_name, log_path, time):
        '''
        saves the used software, the filenames where the data is saved
        and the time
        ''' 

        #generate file and init
        SILogDir = "%s%s_tr-%d_SILog.csv" % (save_file_path, file_name, 
                                             MM_trial.Trials.trial_no)
        fd = open(SILogDir, "a")
        #print(SILogDir," SILogDir")
        writer = csv.writer(
            fd, dialect='excel', quotechar='"', quoting=csv.QUOTE_ALL,
            lineterminator='\n', delimiter=';')
    

        #write control data
        if os.stat(SILogDir).st_size == 0:
            writer.writerow(["software", "filename", "time"])
        writer.writerow(["MM", file_name, time])
        fd.close()
    #

    def data_to_csv(self, save_to, save_file_path, file_name): 
        ''' 
        Takes a dictonary of lists and writes them in a csv file
        if the file already exists and append==True, the data is appended at 
        the end
        '''
        if not save_to == "Nowhere":
            # protection against appending, 
            # not needed with check_against_overwrite()
            file_name = "%s%s%s.csv" % \
                 (save_file_path, file_name, self._file_name_add)

            # print(file_name," save to")

            # define .csv file
            if not os.path.exists(save_file_path) and save_file_path != '':
                os.makedirs(save_file_path)

            # unpack data
            unpacked_data = [i for i in zip(
               *([k] + self.data[k] for k in list(self.data.keys())))] 

            # initialize file
            fd = open(file_name, 'a')
            data_file = csv.DictWriter(
                fd, fieldnames=unpacked_data[0], dialect='excel',
                quotechar='"', quoting=csv.QUOTE_ALL, lineterminator='\n',
                delimiter=';')

            # write header and data in file
            if os.stat(file_name).st_size == 0:
                data_file.writeheader()
        
            # write data           
            for row in range(1, len(unpacked_data)):
                data_file.writerow(
                    dict(zip(unpacked_data[0], unpacked_data[row]))) 
            # close the file object
            fd.close()
            print("%s saved at %s" %(self.data_type, file_name))
        else:
            print("This is a test run without saving data or metadata.")
        return 0

    def add_row(self, **kwords):
        '''
        add given value for given key
        all non-specified entries in this row are set to None
        '''
        for key, value in kwords.items():
            self.data[key].append(value)
    
        for key in list(
                set([k for k in kwords.keys()]) 
                ^ set([k for k in self.data.keys()])):
            self.data[key].append(None)
        #

    def add(self, **kwords):
        '''
        add given value for given keys
        '''   
        for key, value in kwords.items():
            # create field if not found
            if key not in self.data.keys():
                self.data[key] = []
            #
            self.data[key].append(value)

    def update(self, **kwords): 
        '''
        updates values for all given key
        '''
        for key, value in kwords.items():
            self.data[key] = [value]

    def remove_field(self, key):
        '''
        add given value for given keys
        '''
        try:
            del self.data[key]
        except:
            pass
        #

    def replace_data_in_file(self, save_to, save_file_path, file_name, 
                             **kwords): 
        '''
        replace data in already existing csv file
        needs list of values
        '''
        #save block metdata if this did not happen yet
        if not os.path.exists(
                save_file_path + file_name + self._file_name_add + '.csv'):
            self.data_to_csv(save_to, save_file_path, file_name)
    
        #read block metadata from file
        fd = open(
            "%s%s%s.csv" % (save_file_path, file_name, self._file_name_add),
            "r")
        reader = csv.reader(
            fd, dialect='excel', quotechar='"', quoting=csv.QUOTE_ALL,
            lineterminator='\n', delimiter=';')
        stored_metadata = [row for row in reader]
        fd.close

        #change the given metadata variable in already stored data
        for key, value in kwords.items():
            try:
                row_idx = stored_metadata[0].index(key)
                for i in range(len(value)):
                    stored_metadata[i+1][row_idx]= value[i]
            except:
                print("%s:%s was not saved: unexpected metadata key" % 
                      (key, value))
    
        #save data back to the file
        fd = open(
            "%s%s%s.csv" % (save_file_path, file_name, self._file_name_add),
            "w")
        metadata_file = csv.writer(
            fd, dialect='excel', quotechar='"', quoting=csv.QUOTE_ALL,
            lineterminator='\n', delimiter=';')
        for row in stored_metadata:
            metadata_file.writerow(row)
        fd.close()      

    def check_against_overwrite(self, GUI, new_block_ID, new_file_basename):
        """Check if filename already exists and increments the 
        block ID automatically if necessary"""
        
        # check for file directory and file basename
        loop_idx = 0
        while os.path.exists("%s%s_mdata.csv" % \
                (GUI.eDataDir.get(), GUI.eFileBaseName.get())):
            if loop_idx == 0:
                print("***INFO***", 
                      "\nThe chosen file basename already exists.")
            if GUI.setGenSetManually.get():
                mbox.showwarning(
                    'File already exists',
                    'Warning: File already exists. \n \
                    Please choose another file basename.')
                break
            else:
                if loop_idx == 0:
                    print("The data block ID was automatically incremented.")

                # enable changes for eBlockID and eFileBaseName
                GUI.eBlockID.configure(state=tk.NORMAL)
                GUI.eFileBaseName.configure(state=tk.NORMAL)

                # update eBlockID and eFileBaseName
                new_block_ID = int(GUI.eBlockID.get())+1
                GUI.eBlockID.delete(0, tk.END)
                GUI.eBlockID.insert(0, new_block_ID)
                GUI.eFileBaseName.delete(0, tk.END)
                new_file_basename = new_file_basename + str(loop_idx)
                GUI.eFileBaseName.insert(0, new_file_basename)
                # update BlockMetadata with new eBlockID and eFileBaseName
                self.update(BlockID=new_block_ID)
                self.update(FileBaseName=new_file_basename)
                # disable changes for eBlockID and eFileBaseName
                GUI.eBlockID.configure(state='readonly')
                GUI.eFileBaseName.configure(state='readonly')
            loop_idx += 1

    def fill_end(self):
        '''
        fills the empty lines in each data set at the end of each 
        trial with None
        '''
        n = max([len(self.data[key]) for key in self.data.keys()])
        for key, val in self.data.items():
            print(key, val)
            if len(val)<n:
                val.append(None)
            print(key, val)
