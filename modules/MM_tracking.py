#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 21s 2018

@copyright: 2018, Alexander Bexter
"""

import Timing
import var
import time
import numpy as np
import rawinputreader


class Tracking():
    """Object to drive the movement of the VR. Can be counter or ball type"""

    def __init__(self, tracking_device):
        '''
        need tracking device in ['useWheel', 'useBall']
        '''
        self.device = tracking_device
        self.position = []
        Tracking.tracking = False
        self.tracking = False

    def restart(self):
        pass

    def update(self, config, data, GUI, GUI_Behavior, maze, server, trial, TTL, 
               rules, GUI_Rules, stim, GUI_FT, trial_active=True):
        pass
        
    def send_restart(self, server):
    	pass

    def get_mouse_data(self):
        return self.position()

    def send_mouse_data(self, config, data, rules, GUI, GUI_Behavior, maze, 
                        server, trial, TTL, GUI_Rules, stim, GUI_FT):
        for dataline in self.get_mouse_data():
            server.handler.handle_send(dataline)
            
    def __del__(self):
    	'''
    	end usage of tracking device
    	'''
    	del self.device 
    	del self.position
    	del self.tracking
    	#del Tracking.tracking
    	del self


class Counter(Tracking):

    def __init__(self, conf_lines, circumference_in_cm=62.):
        """
        Creates a counter object to read from. This object is a rotary encoder 
        used with a wheel, which is normally connected to a NI card
        """
        Tracking.__init__(self, 'useWheel')
        
        self.lines = conf_lines
        self.position = [0]
        self.circumference_in_cm = circumference_in_cm
        self.actual_position = [0]
        self.autorun_mode = False
        if var.NI:
            import nidaqmx
            from nidaqmx.constants import EncoderType
            self.task = nidaqmx.Task()
            self.task.ci_channels.add_ci_lin_encoder_chan(
                self.lines,
                decoding_type=EncoderType.X_1,
                dist_per_pulse=1)
            self.task.start()
    #

    def __del__(self, destroy=True):
        #close and delete the counter task 
        if var.NI:      
            self.task.stop()
            self.task.close()
            del self.task

        if destroy:
            Tracking.__del__(self)

    def restart(self):
        """
        restart the counter task and reset position
        """
        temp_lines = self.lines
        temp_circumference_in_cm = self.circumference_in_cm
        self.__del__(destroy=False)
        self.__init__(temp_lines, temp_circumference_in_cm)

    def update(self, config, data, GUI, GUI_Behavior, maze, server, trial, TTL, 
               rules, GUI_Rules, stim, GUI_FT, trial_active=True):
        """
        reads the movement of the counter and store it in position.
        if autorun is enabled, the movement is calculated
        automatically without reading the device
        """

        self.actual_position[0] = self.read_wheel_position(config)
        self.autorun_mode = GUI.autorun.get()

        # autorun
        if self.autorun_mode:
            if GUI.useConstantSpeed.get() == 'constant' and trial_active:
                if time.time() > var.autorun_time:
                    var.autorun_time = time.time() + 0.01
                    # The original wheel is d=20cm
                    self.position[0] += \
                        (1024.0/(20*np.pi) * float(GUI.eRunSpeed.get())) / 100
            elif GUI.useConstantSpeed.get() == 'variable' and trial_active:
                if (Timing.timeStrTo_msecs(trial.speedProfile[-1][0]) \
                        - Timing.timeStrTo_msecs(trial.speedProfile[0][0])) \
                            < (Timing.times['session_time'] - \
                            Timing.times['t_start'])*1000.0: #end
                    trial.new_trial(
                        config, self, data, rules, GUI_Rules, GUI, 
                        GUI_Behavior, maze, server, TTL, stim, GUI_FT, 
                        no_loop=False)

                for point in trial.speedProfile:
                    if (Timing.timeStrTo_msecs(point[0]) \
                            - Timing.timeStrTo_msecs(
                                trial.speedProfile[0][0])) <= \
                                (Timing.times['session_time'] - \
                                Timing.times['t_start'])*1000.0:
                        self.position[0] = int(point[1])
        else:  # normal wheel movement (no autorun)
            self.position[0] = self.actual_position[0]

    def read_wheel_position(self, config):
        wheel_position = 0
        if var.NI:
            if config.get('conf', 'EncoderPos') == 'left':
                # The original wheel is d=20cm
                wheel_position = -self.task.read(1)[0] * (self.circumference_in_cm / (20 * np.pi))
            else:
                # The original wheel is d=20cm
                wheel_position = self.task.read(1)[0] * (self.circumference_in_cm / (20 * np.pi))
        return wheel_position

    def send_restart(self, server):
        """Send restart signal to blender to reset the position of the wheel"""
        server.handler.handle_send("?0#")

    def send_mouse_data(self, config, data, rules, GUI, GUI_Behavior, maze, 
                        server, trial, TTL, GUI_Rules, stim, GUI_FT):
        """Sends position of counter to blender"""
        
        from MM_trial import Trials
        if var.NICount != self.position[0]:
            diffCount = self.position[0] - var.NICount
            var.NICount = int(self.position[0])
            # send new data to blender
            server.handler.handle_send("?" + str(-var.NICount) + "#")

            GUI.LStepNo.config(text="Steps: " + str(var.NICount))

            data[0].add_row(
                time=Timing.get_time(), xpos=var.mouse_x, ypos=var.mouse_y,
                steps=var.NICount, actual_position=self.actual_position[0])

            if int(GUI.eLengthTrialDist.get()) <= int(var.NICount) and \
                    int(GUI.eLengthTrialDist.get()) > 0:
                print("COUNTEREND")
                Trials.trial_running = False
                if var.digital_output:
                    TTL.TTLTrigger(1)
                trial.new_trial(
                    config, self, data, rules, GUI_Rules, GUI, GUI_Behavior, 
                    maze, server, TTL, stim, GUI_FT)

            trial.speedDist[int(
                    str(round(GUI.timeDisplay, 1))[-1])] += int(diffCount)
#


class Ball(Tracking):
    
    def __init__(self, circumference_in_cm=62.):
        Tracking.__init__(self, 'useBall')
        # Read the movement of the computer mice
        self.rir = None
        self.circumference_in_cm = circumference_in_cm
        print("UseBall")
        
    def __del__(self):
        self.rir.stop()
        del self.rir
        Tracking.__del__(self)

    def get_mouse_data(self, MouseID1, MouseID2):
        """
        read movement of computer mice
        :param MouseID1: string
        :param MouseID2: string
        :return: list of strings
        """
        if self.rir == None:
            self.rir = rawinputreader.rawinputreader()
            
        Output = self.rir.pollEvents()
        self.position = []
        for element in Output:
            if element[0] == None:
                OutID = 0
            elif int(element[0]) == int(MouseID1):
                OutID = 1
            elif int(element[0]) == int(MouseID2):
                OutID = 2
            else:
                OutID = 0
            self.position.append("!%d*%g*%g*#" %(
                                 OutID, element[6], element[7]))
    
        # self.rir.empty_events()
        return self.position

    def send_mouse_data(self, config, data, rules, GUI, GUI_Behavior, maze, 
                        server, trial, TTL, GUI_Rules, stim, GUI_FT):
        """
        Sends the computer mouse movement to blender
        """
        for dataline in self.get_mouse_data(
                config.get('conf', 'mouseid1'),
                config.get('conf', 'mouseid2')):
            server.handler.handle_send(dataline)

