# -*- coding: utf-8 -*-
"""
Created on Wed Apr 17 14:57:02 2019

@author: bexter
"""
import var
import Timing

class TTL:
    
    def __init__(self, config):
        """
        Handle the start and stop trigger for a microscope (e.g. scanimage)
        or some other device
        """
        
        self.digitalTrigTasks = []
        self.triggerTime = []
    
        if var.NI:
            import nidaqmx

            #trigger A
            self.digitalTrigTasks.append(nidaqmx.Task())
            self.digitalTrigTasks[-1].do_channels.add_do_chan(
                config.get('NIlines', 'trigger A'), 
                name_to_assign_to_lines='trigger A')
            self.triggerTime.append(0.0)
            
            #trigger B
            self.digitalTrigTasks.append(nidaqmx.Task())
            self.digitalTrigTasks[-1].do_channels.add_do_chan(
                config.get('NIlines', 'trigger B'), 
                name_to_assign_to_lines='trigger B')
            self.triggerTime.append(0.0)
            
            #trigger C
            self.digitalTrigTasks.append(nidaqmx.Task())
            self.digitalTrigTasks[-1].do_channels.add_do_chan(
                config.get('NIlines', 'trigger C'), 
                name_to_assign_to_lines='trigger C')
            self.triggerTime.append(0.0)
            
            #trigger D
            self.digitalTrigTasks.append(nidaqmx.Task())
            self.digitalTrigTasks[-1].do_channels.add_do_chan(
                config.get('NIlines', 'trigger D'), 
                name_to_assign_to_lines='trigger D')
            self.triggerTime.append(0.0)

    def TTLTrigger(self, line, TriggerTime=0.01):
        """Start the given trigger
        and set the duration of the TTL pulse"""

        if line >=0 and line <= 3:
            self.digitalTrigTasks[line].write(True, auto_start=True)
            self.digitalTrigTasks[line].stop()

        else:
            print("No valid Trigger line given")
            return
        
        self.triggerTime[line] = Timing.times['session_time'] + TriggerTime


    def trigger_update(self):
        """check for each trigger if duration of trigger exceeded and
        if this is the case, set trigger port to False"""
        if var.NI:
            for no in range(4):
                if self.triggerTime[no] > 0 and \
                Timing.times['session_time'] >= self.triggerTime[no]:
                    self.digitalTrigTasks[no].write(False, auto_start=True)
                    self.digitalTrigTasks[no].stop()
                    self.triggerTime[no] = 0
                    
    def close(self):
        """close and delete the start and stop tasks"""
        if var.NI:
            for no in range(4):
                self.digitalTrigTasks[0].stop()
                self.digitalTrigTasks[0].close()
                del self.digitalTrigTasks[0]
