# -*- coding: utf-8 -*-
from MM_trial import Trials
import var
import Timing
import time
import ast
from GUI_scripts import GUIScripts

def reset_cue(canvas, cue_marker):
    canvas.coords(cue_marker, 0, 0, 0, 0)  # remove cue marker

def teleport(config, tracking_device, data, GUI, maze, server, 
             new_trial_teleport, TTL, trial, rules, GUI_Behavior, 
             GUI_Rules, stim, GUI_FT):      
    """
    initialized teleportation to teleporter point. If "new trial after
    teleport" option is enabled, a new trial is started. 
    """
    
    #überflüssig?
    telePos = ast.literal_eval(maze.data.get('Positions','teleport'))
    server.handler.handle_send("+%f#%f#" % \
                                   (float(telePos[0]),float(telePos[1])))  
    
    if config.get('conf', 'useWheel') == 'True':
        tracking_device.restart()
        tracking_device.send_restart(server)

    else:
        telePos = ast.literal_eval(maze.data.get('Positions','teleport'))
        server.handler.handle_send("+%f#%f#" % \
                                   (float(telePos[0]),float(telePos[1])))            

    if new_trial_teleport:
        #server.handler.handle_send("+NEWTRIAL#")
                                   
        Trials.trial_running = False
        #if not GUI.TMaze.get():
        #    data[1].add(Answer_side="None")

        data[0].save_data(
            data, GUI.saveFilesTo.get(), GUI.eDataDir.get(), 
            GUI.eFileBaseName.get(), GUI.timeDisplay, Timing.get_time(), GUI)
        #server.handler.handle_send("+NEWTRIAL#")
        trial.new_trial(
            config, tracking_device, data, rules, GUI_Rules, GUI, GUI_Behavior, 
            maze, server, TTL, stim, GUI_FT, no_loop=False)
        
        if GUI.cuesBackground.get() and stim.isOpen():
            server.handler.handle_send("+NEWTRIAL#"+str(Trials.trial_no)+"#")
        else:
            server.handler.handle_send("+NEWTRIAL#")
                                       
                                       
def end_TM_trial(GUI, trial_meta_data, TMaze_end, NoSensor, sec):
    if NoSensor == TMaze_end[0]:
        if var.Tmaze_side == 'Left':  # correct
            TMaze_correct = True
            GUI.AnswerLED_L.config(bg="#86b300")  # green

        elif var.Tmaze_side == 'Right':  # false
            Timing.times['TM_end_time'] = Timing.times['sec']
            Timing.times['TM_end_timeak_time'] = time.time()
            TMaze_correct = False
            GUI.AnswerLED_L.config(bg="#9d3631")  # red

        trial_meta_data.add(Answer_side="Left")


    elif NoSensor == TMaze_end[1]:
        if var.Tmaze_side == 'Right':  # correct
            TMaze_correct = True
            GUI.AnswerLED_R.config(bg="#86b300")  # green
        elif var.Tmaze_side == 'Left':  # false
            Timing.times['TM_end_time'] = Timing.times['sec']
            Timing.times['TM_end_timeak_time'] = time.time()
            TMaze_correct = False
            GUI.AnswerLED_R.config(bg="#9d3631")  # red

        trial_meta_data.add(Answer_side="Right")

    Trials.answers.append([var.Tmaze_side, TMaze_correct])

def set_sensor(config, tracking_device, data, GUI, GUI_Rules, maze, rules, 
               server, stim, new_trial_teleport, rulesOpen, sensorNo, 
               sensorOnce, TTL, trial, GUI_Behavior, GUI_FT):
    """
    Sensor activated by passing it.
    Check for rules corresponding to the sensor number and execute them
    """
    
    if maze.sensorWallInactive[sensorNo] == 0:  # active
        GUI.Canvas.itemconfig(maze.sensorWall[sensorNo], fill="#2E0854")

        data[0].add_row(
            time=Timing.get_time(), xpos=var.mouse_x, ypos=var.mouse_y,
            sensor_barrier_ID=sensorNo)

        """
        # checking for end the Tmaze trial
        if GUI.Tmaze.get() and Timing.times['TM_end_time'] == 0:
            for sensorentry in maze.TmazeEnd:
                if sensorentry == int(sensorNo)and not Trials.trialEnd:
                    # end trial in 3 sec
                    end_TM_trial(
                        GUI, data[1], maze.TmazeEnd, sensorentry, 
                        float(GUI.eTMazeEndTime.get()))
                    Trials.trialEnd = True

        """   
        #check for flash tunnel air puffs
        if GUI_FT.isOpen():
            if GUI_FT.useAirPuffs.get():
                (AirPuffleft, AirPuffright) = GUI_FT.checkAirPuffs(sensorNo)
                if AirPuffleft:
                    stim.apply_air_puff(GUI, data[0], 'left')
                if AirPuffright:
                    stim.apply_air_puff(GUI, data[0], 'right')   
        
     
        if rulesOpen:  
            for ruleNo in range(len(rules.rules)): #Check all rules possible
                # matching rule to 1st sensor
                if sensorNo == GUI_Rules.getSensors(ruleNo)[0]:
                    #only one(first) sensor to trigger the cue 
                    if GUI_Rules.getSensors(ruleNo)[1] == -1: 
                        
                        stim.set_cue(server, data[0], GUI.eCueTime.get(), 
                                     GUI_Rules.getCues(ruleNo)[0], 
                                     GUI_Rules.getCues(ruleNo)[1])
                        
                        
                        if GUI_Rules.getFalseWallNo(ruleNo) != -1:  #False Wall
                            stim.activate_false_wall(
                                    GUI_Rules.getFalseWallNo(ruleNo), server)
                        if GUI_Rules.getTeleport(ruleNo): #teleport
                            teleport(config, tracking_device, data, GUI, maze, 
                                     server, new_trial_teleport, TTL, trial, 
                                     rules, GUI_Behavior, GUI_Rules, stim, 
                                     GUI_FT)
                            # stim.teleport( ###---> no teleport in stim
                            #     exp, GUI.Canvas, window, data, server, 
                            #     counter, maze)

                    else: #wait for second sensor -> set first sensor to active
                        GUI_Rules.get_setRuleActive(ruleNo, change=True)
                # match 2nd sensor        
                if sensorNo == GUI_Rules.getSensors(ruleNo)[1]:  
                    if GUI_Rules.get_setRuleActive(ruleNo):
                        stim.set_cue(
                            server, data[0], GUI.eCueTime.get(), 
                            cueNoL=GUI_Rules.getCues(ruleNo)[0], 
                            cueNoR=GUI_Rules.getCues(ruleNo)[1])
                        if GUI_Rules.getFalseWallNo(ruleNo) != -1:  #False Wall
                            stim.activate_false_wall(ruleNo[5])
                        if GUI_Rules.getTeleport(ruleNo):
                            teleport(config, tracking_device, data, GUI, maze, 
                                     server, new_trial_teleport, TTL, trial, 
                                     rules, GUI_Behavior, GUI_Rules, stim, 
                                     GUI_FT)
                            
                           
        # TODO: check if nessesary 
        elif maze.data.get('settings','endless') == 'True' and \
                GUI.useConstantSpeed.get() != 'variable' and \
                rules.teleport_active and \
                Timing.times['sensor_delay'] == 0.0 and sensorNo==0:
            rules.teleport_active = False
            Timing.times['sensor_delay'] = time.time() + 1.5
            #special default rule for teleport 
            #in the endless tunnel
            teleport(config, tracking_device, data, GUI, maze, server, 
                     new_trial_teleport, TTL, trial, rules, GUI_Behavior, 
                     GUI_Rules, stim, GUI_FT)

    if sensorOnce:
        try:
            SNumdelay = float(config.get('conf', 'sensorDelay'))
        except:
            print("ERROR: no valid number in delay")
            SNumdelay = 5
        maze.sensorWallInactive[sensorNo] = time.time() + SNumdelay #delay(sec)
    else:
        maze.sensorWallInactive[sensorNo] = -1

def set_reward(rewardNo, newTrialReward, trial, stim, data, GUI, TTL, config, 
               tracking_device, rules, GUI_Behavior, maze, server, GUI_Rules,
               GUI_FT, behavior):
        if not (rewardNo in trial.reward_used) \
                and time.time() > Timing.times['reward_delay'] \
                and behavior.answerWindow == 0 and behavior.rewardWindow == 0 \
                and not Timing.times['wait_for_trigger']:
                    
            try:
                maze.canvas.itemconfig(maze.rewardZone[rewardNo], fill="#2E0854")
            except:
                print("no change in reward color possible. no ", rewardNo)

            data[0].add_row(
                time=Timing.get_time(), xpos=var.mouse_x, ypos=var.mouse_y, 
                reward_ID=rewardNo)
            #print("Reward: ", rewardNo)
            
            #TMaze
            if maze.tmaze:
                if int(rewardNo)==0:
                    behavior.answerTrial('left', behavior.target, GUI, data, 
                                         stim, GUI_Behavior)
                elif int(rewardNo)==1:
                    behavior.answerTrial('right', behavior.target, GUI, data, 
                                         stim, GUI_Behavior)
            
            if bool(int(newTrialReward)):
                # Start a new Trial!
                Timing.times['TM_end_time'] = 0
                Timing.times['TM_end_timeak_time'] = 0

                # Stop Camera
                if var.digital_output and GUI.pupilCam.get():
                    var.DEyetracker.write(0, auto_start=True)
                    var.DEyetracker.stop()
                Trials.trial_running = False
                #if not GUI.TMaze.get():
                #    data[1].add(Answer_side=None)

                #if var.digital_output:
                #    TTL.TTLTrigger(1)
                    
                if GUI.autoReward.get():
                    stim.give_reward(
                        data[0], float(GUI.eValveOpen.get()), 
                        float(GUI.eRewProba.get()), behavior.target)
                    trial.reward_used.append(rewardNo)
                else:
                    #data[0].save_data(
                    #    data, GUI.saveFilesTo.get(), GUI.eDataDir.get(), 
                    #    GUI.eFileBaseName.get(), GUI.timeDisplay, 
                    #    Timing.get_time(), GUI)
                    
                    #trial.reward_used.append(rewardNo)
                    if GUI_Behavior.isOpen():
                        if GUI_Behavior.taskEnabled.get() and \
                        behavior.answerWindow==0:
                            behavior.answerWindow = time.time() + \
                                float(GUI_Behavior.eAnswerWindow.get())  
                            
                            #check for script
                            for scripts in GUIScripts.scripts:
                                if scripts['trigger'].get() == \
                                'Start of Answer Period':
                                    GUIScripts.executeScript(scripts)
                
                
                    else:
                        data[0].save_data(
                            data, GUI.saveFilesTo.get(), GUI.eDataDir.get(), 
                            GUI.eFileBaseName.get(), GUI.timeDisplay, 
                            Timing.get_time(), GUI)
                        
                        trial.new_trial(config, tracking_device, data, rules, 
                                        GUI_Rules, GUI, GUI_Behavior, maze, 
                                        server, TTL, stim, GUI_FT)
            else:
                if GUI.autoReward.get():         
                    stim.give_reward(
                        data[0], float(GUI.eValveOpen.get()), 
                        float(GUI.eRewProba.get()), 'R')
                    trial.reward_used.append(rewardNo)
                    Timing.times['reward_delay']

            if GUI_Behavior.isOpen():
                Timing.times['reward_delay'] = time.time()+2+\
                    float(GUI_Behavior.eAnswerWindow.get())
            else:
                Timing.times['reward_delay'] = time.time()+2   
            #trial.reward_used.append(rewardNo)

class Communication():

    def read_TCP_input(config, tracking_device, data, GUI, GUI_Rules, 
                       GUI_Behavior, maze, rules, server, stim, trial, TTL, 
                       new_trial_teleport, GUI_FT, behavior, rulesOpen=False):
        inputString = server.handler.readData
        for n, ch in enumerate(inputString):
            if ch == '!': # Mouse Position
                if GUI.connectTo.get() == 'Blender': 
                    pos1 = inputString.find('#',n)
                    try:
                        maze.pos_mouse(
                            float(inputString[n+1:pos1]),
                            float(inputString[pos1+1:inputString.find('#',
                                                                     pos1+1)]))
                        #save position as global variables
                        var.mouse_x = round(float(inputString[n+1:pos1]),2)
                        var.mouse_y = round(float(inputString[
                                pos1+1:inputString.find('#',pos1+1)]),2)                                                  
                    except:
                        print("position error")
                    

                elif GUI.connectTo.get() == 'PsychoPy': 
                    counter_steps = int(inputString[n+1:])
                    GUI.LStepNo.config(text="Steps: %d" % counter_steps)
                    if Trials.trial_running:
                        data[0].add_row(
                            time= Timing.get_time(), xpos=var.mouse_x, 
                            ypos=var.mouse_y, steps=counter_steps)

                    if 0 < int(GUI.eLengthTrialDist.get()) <= counter_steps:
                        Trials.trial_running = False
                        data[0].save_data(
                            data, GUI.saveFilesTo.get(), 
                            GUI.eDataDir.get(), GUI.eFileBaseName.get(),
                            GUI.timeDisplay, Timing.get_time(), GUI)
                        trial.new_trial(
                            config, tracking_device, data, rules, GUI_Rules, 
                            GUI, GUI_Behavior, maze, server, TTL, stim, GUI_FT)

            elif ch == '$': # Cue Position
                pos1 = inputString.find('#',n)
                maze.position_cue(
                    GUI.Canvas, 
                    float(inputString[n+1:pos1]), #float(inputString[pos1+1:]))
                    float(inputString[pos1+1:inputString.find('#',pos1+1)]))
                    
            elif ch == '%': # Cue echo
                reset_cue(GUI.Canvas, maze.cueMarker)
                
            elif ch == '§':  # sensor hit
                if len(inputString[n+1:])>1:
                    try:
                        sensorNo = int(inputString[n+1:n+3])
                    except:
                        sensorNo = int(inputString[n+1])
                else:
                    sensorNo = int(inputString[n+1])
                    
                set_sensor(
                    config, tracking_device, data, GUI, GUI_Rules, maze, rules,
                    server, stim, new_trial_teleport, rulesOpen, sensorNo, 
                    bool(config.get('conf', 'sensorReUse')), TTL, trial, 
                    GUI_Behavior, GUI_FT) ###--->  sensor Once?
           
            elif ch == '&': # Reward
                if len(inputString[n+1:])>1:
                    try:
                        rewardNo = int(inputString[n+1:n+3])
                    except:
                        rewardNo = int(inputString[n+1])
                else:
                    rewardNo = int(inputString[n+1])

                set_reward(rewardNo, config.get('conf', 'newTrialReward'), 
                           trial, stim, data, GUI, TTL, config, 
                           tracking_device, rules, GUI_Behavior, maze, server, 
                           GUI_Rules, GUI_FT, behavior)
                
            elif ch == '}':  # New Trial
                # Restart Tracking
                StartCoords = GUI.Canvas.coords(maze.startPosMarker)
                maze.build_maze()
                continue
