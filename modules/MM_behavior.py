import time
import var
import random
from GUI_scripts import GUIScripts

class Behavior():
    
    answers = []
    target = None
    
    def __init__(self, lines):
        """
        initializes two tasks, represengting two lick sensor lines for 
        lick detection in a behavior task.
        """
        if var.NI:
            import nidaqmx
            self.licktaskR = nidaqmx.Task()
            self.licktaskR.di_channels.add_di_chan(
                lines[0], name_to_assign_to_lines='LickR')
            self.licktaskR.start()
            
            self.licktaskL = nidaqmx.Task()
            self.licktaskL.di_channels.add_di_chan(
                lines[1], name_to_assign_to_lines='LickR')
            self.licktaskL.start()
            
            self.lickDelay = time.time()
            
        self.answerWindow = 0
        self.rewardWindow = 0
        
    def lick_update(self, targetSide, GUI, data, stim, GUI_Behavior):
        """
        checks all lick tasks for events. A delay time is used for checking this task.
        return left or right as a string, depending on the side(task), 
        where activity was found. 
        """
        out = None
        if var.NI:
            lick_inputR = self.licktaskR.read()
            if time.time() > self.lickDelay:
                if lick_inputR:  # Digital Signal                   
                    self.lickDelay = time.time() + 0.2  # sec delay
                    #print("LickR")
                    if self.answerWindow > 0:
                        self.answerWindow = 0
                        self.answerTrial('right', targetSide, GUI, data, stim, 
                                         GUI_Behavior)
                    out = 'right'
                    
            lick_inputL = self.licktaskL.read()
            if time.time() > self.lickDelay:
                if lick_inputL:  # Digital Signal                    
                    self.lickDelay = time.time() + 0.2  # sec delay
                    #print("LickL")
                    if self.answerWindow > 0:
                        self.answerWindow = 0
                        self.answerTrial('left', targetSide, GUI, data, stim, 
                                         GUI_Behavior)      
                    out = 'left'
        return out
    
    
    def behavior_update(self, data):
        """Checks for timeout or reward end and returns true for starting a
        new trial"""
        #answertime timeout
        if self.answerWindow > 0:
            if time.time() >= self.answerWindow:
                self.answerWindow = 0
                self.rewardWindow = 0
                data[1].add(Answer_side="None")
                Behavior.answers.append([Behavior.target,'None'])
                #check for script
                for scripts in GUIScripts.scripts:
                    if scripts['trigger'].get()=='End of Answer Period' or \
                        scripts['trigger'].get()=='On Missed':
                        GUIScripts.executeScript(scripts)
                return True

        if self.rewardWindow > 0:
            if time.time() >= self.rewardWindow:
                self.rewardWindow = 0
                self.answerWindow = 0
                return True

    def calc_behavior(answers_in):
        """
        creates a list (left correct, right correct, left wrong, right wrong)
        with numbers based on previous answered trials (answers_in).
        """
        out = [0, 0, 0, 0]  # leftc,rightc,leftw,rightw
        for i in answers_in:
            if i[1]==True or i[1]==1 or i[1]=='1'or i[1]=='True':  # correct
                if i[0] == 'right':
                    out[1] += 1
                elif i[0] == 'left':
                    out[0] += 1
            elif i[1]==False or i[1]==0 or i[1]=='0'or i[1]=='False':
                if i[0] == 'right':
                    out[2] += 1
                elif i[0] == 'left':
                    out[3] += 1           
        return out
    
    def bias_correction(answers, correct=True):
        '''
        reduces bias determined decisions
        if last and second last answers where wrong and at 
        same side: again same side
        '''
        random.seed()
        side = random.choice(['right', 'left'])
        # _randSide = random.getrandbits(1)
        # if _randSide == 0:      
        #   side = 'Right'
        # else:
        #   side = 'Left'
        if len(answers)>1 and correct:
            if answers[-1][1] == False and answers[-1][0] != 'None': 
                #last answer wrong
                if answers[-2][1] == False and answers[-2][0] != 'None': 
                    #second last answer also wrong
                    if answers[-2][0] == answers[-1][0]: #same side 
                        _randSide = random.randint(1, 10)
                        if _randSide < 9: # 80 % chance to get same side again
                            print("Bias Corr activated: reward side \
                                  same as before")
                            side = answers[-2][0]
                        else:
                            print("Bias Corr activated: \
                                reward side changed (motivation)")
                            if answers[-2][0] == 'Left':
                                side = 'right'
                            else:
                                side = 'left'
        return side
    
    
    def answerTrial(self, answerSide, targetSide, GUI, data, stim,           
                    GUI_Behavior):
        """
        sets the answer to a trial during a behavior task. Save the answer and
        give areward if necessary. Check for script triggers.
        """ 
    
        _correct = None
        if answerSide == 'left':
            if targetSide == 'left':   # correct 
                stim.give_reward(data[0], float(GUI.eValveOpen.get()), 
                                 float(GUI.eRewProba.get()), 'L')
                
                
                GUI_Behavior.AnswerLED_L.config(bg="#86b300") #green
                _correct = True
            elif targetSide == 'right':   # false
                GUI_Behavior.AnswerLED_L.config(bg="#9d3631") #red
                _correct = False  
 
            data[1].add(Answer_side="left")    
    
                      
        elif answerSide == 'right':
            if targetSide == 'right':   # correct
                stim.give_reward(data[0], float(GUI.eValveOpen.get()), 
                                 float(GUI.eRewProba.get()), 'R')
                GUI_Behavior.AnswerLED_R.config(bg="#86b300") #green
                _correct = True
            elif targetSide == 'left':   # false
    
                GUI_Behavior.AnswerLED_R.config(bg="#9d3631") #red 
                _correct = False
                
            data[1].add(Answer_side="right")
                          
        Behavior.answers.append([targetSide,_correct])
        self.rewardWindow = time.time() + \
                            float(GUI_Behavior.eRewardWindow.get())
        self.answerWindow = 0

        #check for script
        for scripts in GUIScripts.scripts:
            if scripts['trigger'].get()=='End of Answer Period':
                GUIScripts.executeScript(scripts)
                
        #check for script -error                    
        if _correct==False:
            for scripts in GUIScripts.scripts:
                if scripts['trigger'].get()=='On Error':
                    GUIScripts.executeScript(scripts) 
            
    
    def close(self):
        if var.NI:
            self.licktaskR.stop()
            self.licktaskR.close()
            del self.licktaskR
            self.licktaskL.stop()
            self.licktaskL.close()
            del self.licktaskL
