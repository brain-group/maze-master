# -*- coding: utf-8 -*-
"""
Created on Tue July 17 10:24:01 2018

@author: bexter
"""

import random
import time
import Timing
import tkinter as tk
import csv

import var
from MM_behavior import Behavior
from MM_tracking import Tracking
from GUI_scripts import GUIScripts

# ==============================================================================
# Trials Class
# ==============================================================================


def int_list_to_str(input):
    output = ''
    for val in input:
        output += str(val)
    #

    return output
#


class Trials:
    trial_no = -1
    trial_running = False
    trialEnd = False
    maxtrial_no = 0
    speedfiles = []

    def __init__(self):
        self.accSpeed = 0 
        self.trial_time = 0.0
        self.reward_used = []
        self.speedDist = [0 for i in range(10)]
        self.speedProfile = []

        Trials.trial_running = False
        Trials.trial_end = False
        Trials.trial_no += 1 

    def new_trial(self, config, tracking_device, data, rules, GUIrules, GUI, 
                  GUI_Behavior, maze, server, TTL, stim, GUI_FT, save=True, 
                  no_loop=True):
        """Start a new trial. Resets devices, saves data."""
        
        #save the data
        from MM_data_container import DataContainer
        #reset mouse position and set trial to non-running
        Trials.trial_running = False
        var.mouse_x = None
        var.mouse_y = None
        
        if save:
            data[0].save_data(
                data, GUI.saveFilesTo.get(), GUI.eDataDir.get(), 
                GUI.eFileBaseName.get(), GUI.timeDisplay, Timing.get_time(), 
                GUI)

        if no_loop:
            GUI_Behavior.update_behavior()
            GUI.LStartLED.config(bg="#9d3631", text="Trial Waiting")
            random.seed()
        #else:
        #    Trials.trial_running = False

        try:
            Trials.maxtrial_no = int(GUI.eNoTrials.get())
        except:
            print("ERROR: No. of Trials must be a number!")
            Trials.maxtrial_no = 0

        # digital triggers -end of trial
        if var.digital_output:
            for no in range(4):
                if GUI.trigger[no] == "start of trial":
                    TTL.TTLTrigger(no)


        if server.running:
            if no_loop:
                tracking_device.send_restart(server)

            # start new trial if communication with blender is 
            # running and not too many trials
            if (Trials.trial_no < Trials.maxtrial_no-1) or Trials.maxtrial_no == 0:
                if no_loop:
                    self.__init__()
                    Tracking.tracking = False
                    server.handler.handle_send('}')
                    self.reward_used = []
                else:
                    self.reward_used = []
                    Trials.trial_no += 1
                    data[1].add(trial_start=Timing.get_time())

                # update data
                print('Starting Trial No %s' % Trials.trial_no)
                data[1].add(trial_ID=Trials.trial_no)
                data[2].update(Totaltrial_no=Trials.trial_no)
                DataContainer.save_temp_config(GUI)

                # check scripts
                for scripts in GUIScripts.scripts:
                    if scripts['trigger'].get() == 'Start of Trial':
                        GUIScripts.executeScript(scripts)
                
                # FLASH TUNNEL RESET
                if GUI_FT.isOpen():
                    Behavior.target = GUI_FT.randomSide(GUI.biasCorr.get(), 
                                                        Behavior.bias_correction(Behavior.answers))
                    # update behavior
                    GUI_Behavior.update_answers(Behavior.target, None)

                    # read target_seq from the GUI FT
                    current_target_visual = GUI_FT.eTargetOrder.get()

                    target_side = Behavior.target
                    modality = GUI_FT.calcModality(Behavior.answers)

                    # create corridor stimuli and get order
                    # This internally defines the: stim.target_list_visual, stim.distractor_list_visual,
                    #     stim.target_list_tactile, stim.distractor_list_tactile
                    current_target_visual, current_distractor_visual, current_target_tactile,\
                        current_distractor_tactile = stim.updateMultipleTextures(Trials.trial_no,
                                                                                 config,
                                                                                 target_side,
                                                                                 current_target_visual,
                                                                                 modality)

                    data[1].add(Target_side=target_side)
                    data[1].add(modality=modality)

                    # Save the current targets and distractors according to the sequence mode
                    if stim.modality_wise_given:
                        # add all 4 variables
                        # remove the regulat variables
                        data[1].add(target_visual=int_list_to_str(reversed(current_target_visual)))
                        data[1].add(distractor_visual=int_list_to_str(current_distractor_visual))
                        data[1].add(target_tactile=int_list_to_str(reversed(current_target_tactile)))
                        data[1].add(distractor_tactile=int_list_to_str(current_distractor_tactile))

                        data[1].remove_field('target')
                        data[1].remove_field('distractor')
                    else:
                        # remove all 4 variables
                        # add the regulat variables
                        data[1].remove_field('target_visual')
                        data[1].remove_field('distractor_visual')
                        data[1].remove_field('target_tactile')
                        data[1].remove_field('distractor_tactile')

                        data[1].add(target=int_list_to_str(reversed(current_target_visual)))
                        data[1].add(distractor=int_list_to_str(current_distractor_visual))
                    #

                    GUI_FT.setAirPuffs(current_distractor_tactile, current_target_tactile, target_side)
                else:
                    data[1].add(target_visual=None)
                    data[1].add(distractor_visual=None)
                    data[1].add(target_tactile=None)
                    data[1].add(distractor_tactile=None)
                    data[1].add(modality=None)
                # control maze in blender 
                server.handler.handle_send('^' + GUI.eRunSpBlender.get() + '#')

                if no_loop:
                    time.sleep(float(GUI.eDelayTrial.get()) / 2)
                    if var.gratings:
                        server.handler.handle_send(
                            '~' + str(var.grat_frequ[Trials.trial_no][0]) + \
                            '#' + str(var.grat_frequ[Trials.trial_no][1]))
                    else:
                        server.handler.handle_send('~1#1')
                    time.sleep(float(GUI.eDelayTrial.get()) / 2)
                
                # update GUI
                for rewards in maze.rewardZone:
                    GUI.Canvas.itemconfig(rewards, fill='#1C86EE')
                GUI.Canvas.update()

                # Update Simulus List
                if stim.isOpen():
                    data[1].add(sequence_file= stim.stimfile)
                    if maze.data.get('settings', 'endless') == 'True': 
                        stim.updateStimList(Trials.trial_no, len(maze.sensorWall)-1)
                    else:
                        try:
                            stim.updateStimList(Trials.trial_no, len(current_distractor_visual))
                        except AttributeError as e:
                            tk.messagebox.showwarning(title="Can't Start Trial",
                                                      message="Try opening the FlashTunnel window")
                            # print('try opening the FlashTunnel window.')
                            raise e
                        #
               
                    if GUI.flashCues.get():
                        noSensors = len(maze.sensorWall)
                        
                        for n in range(noSensors)[1:]:
                            stimlistIndex = (Trials.trial_no-1) * \
                                            (noSensors-1) + n-1
                            
                            _ruleDict = {"sensor_var1": n, "teleport": False,
                                         "rule_cue_direction": "both",
                                         "cuesVar1": stim.stimlist[stimlistIndex],
                                         "cuesVar2": stim.stimlist[stimlistIndex]}
                            rules.define_Rule(n, _ruleDict, GUIrules) 
                else:
                    data[1].add(sequence_file=None)
                    
                    
                # load speed profile
                if GUI.useConstantSpeed.get() == 'variable' and \
                        GUI.autorun.get():
                    if len(self.speedfiles) > Trials.trial_no:
                        self.loadSpeed_data(Trials.trial_no, data[1])
                    else:
                        Trials.trial_running = False
                        print('Experiment ended')
                        return
                else:
                    data[1].add(Replay_file=None)

                # trigger handling
                if no_loop:
                    if GUI.useStartTrigger.get() == 'Trigger':
                        Timing.times['wait_for_trigger'] = True
                    elif GUI.useStartTrigger.get() == 'Auto':
                        GUI.LStartLED.config(bg="#86b300", 
                                             text="Trial Running")
                        server.handler.handle_send('+')
                        data[0].add_row(
                            time=Timing.get_time(), xpos=var.mouse_x, 
                            ypos=var.mouse_y)
                        data[1].add(trial_start=Timing.get_time())
                        # Create new Task for tracking_device
                        tracking_device.restart()

                        # digital triggers
                        if var.digital_output:
                            for no in range(4):
                                if GUI.trigger[no] == "start of trial":
                                    TTL.TTLTrigger(no)  
                                elif GUI.trigger[no] == "start of experiment":
                                    if Trials.trial_no == 1:
                                        TTL.TTLTrigger(no)  

                        Trials.trial_running = True
                        Timing.times['tracking_time'] = time.time()
                    else:
                        print("ERROR: set start trigger")

                    # get a random direction for target cue in a TMaze
                    if bool(int(config.get('conf', 'tmaze'))):
                        rand_size = Behavior.bias_correction(Behavior.answers)
                        var.Tmaze_side = rand_size[0]
                        server.handler.handle_send(rand_size[1])
                        print("Reward side: %s" % var.Tmaze_side)
                        if var.Tmaze_side == 'Right':
                            GUI.StimLED_R.config(bg="#86b300")  # green
                            GUI.StimLED_L.config(bg="#9d3631")  # red
                        elif var.Tmaze_side == 'Left':
                            GUI.StimLED_L.config(bg="#86b300")  # green
                            GUI.StimLED_R.config(bg="#9d3631")  # red

                        data[1].add(Target_side=var.Tmaze_side)
                    
                    elif not GUI_FT.isOpen():
                        data[1].add(Target_side=None)
                        data[1].add(Answer_side=None)
                else:
                    if not bool(int(config.get('conf', 'tmaze'))) and not \
                    GUI_FT.isOpen():
                        data[1].add(Target_side=None)
                        data[1].add(Answer_side=None)
                    
                    if var.digital_output and GUI.pupilCam.get():
                        var.DEyetracker.write(False, auto_start=True)
                        var.DEyetracker.stop()
                    data[0].add_row(
                        time=Timing.get_time(), xpos=var.mouse_x, 
                        ypos=var.mouse_y)

                    # digital triggers
                    if var.digital_output:
                        for no in range(4):
                            if GUI.trigger[no] == "start of trial":
                                TTL.TTLTrigger(no)    
                            elif GUI.trigger[no] == "start of experiment":
                                if Trials.trial_no == 1:
                                    TTL.TTLTrigger(no)

                # update log file
                if GUI.eSILogDir.get() != "":
                    DataContainer.write_control_trial_data(
                        GUI.eDataDir.get(), GUI.eFileBaseName.get(), 
                        GUI.eSILogDir.get(), 
                        Timing.get_time(
                                time_in_sec=Timing.times['tracking_time']))

            else:
                time.sleep(0.5)
                server.stopServer(data, GUI, self)
                print('Experiment ended')
                return 1

        else:
            tk.messagebox.showinfo("ERROR", "Server is not running yet")
            
        GUI.LTrialNo.config(text="Trial No:  %s" % Trials.trial_no)
        if not no_loop:
            rules.teleport_active = True
        Timing.times['t_start'] = Timing.times['session_time']
            
        self.reward_used = []
        Trials.trial_running = True
 
    def loadSpeed_files(self, randomSpeed):
        """Opens a dialog window to load the speed profile csv files 
        from previous runs and store them in speedfiles and returns them"""
        import random
        speedFiles = tk.filedialog.askopenfilename(
                                    initialdir ="C:\Data\VirtualTunnel\RUN",
                                    multiple=True,
                                    title="Choose a data file",
                                    defaultextension = '.csv',
                                    filetypes=[('csv file','*.csv')])
        if randomSpeed == False:
            speedFiles = sorted(speedFiles)
        else:
            random.shuffle(speedFiles)
        Trials.speedfiles = speedFiles
        print("done loading loadSpeed_files")
        return speedFiles 

    def loadSpeed_data(self, trial_no, trial_meta_data):
        """reads the content (timepoints and steps) 
        from one csv file and returns it"""
        
        filepath = Trials.speedfiles[trial_no-1]
        
        trial_meta_data.add(Replay_file=filepath)
        out=[]
        print(filepath)
        with open(filepath) as csvfile:
            csv_file = csv.DictReader(csvfile, dialect='excel',
                                    quotechar='"', quoting=csv.QUOTE_ALL,
                                    lineterminator='\n', delimiter=';')
            for row in csv_file:
                if row['time'] != '0' and int(row['steps']) != -1:
                    out.append((row['time'], row['steps']))
            csvfile.close()
            self.speedProfile = out
            return out   
