#misc
import time
import datetime
import Timing
import var

#import MM_trial
#from MM_trial import Trials
from MM_tracking import Tracking

def start_tracking(data, maze):
    from MM_trial import Trials
    if not Tracking.tracking:
        print('Start Tracking')
        Tracking.tracking = True
        Timing.times['sec'] = 0
        Timing.times['sec_time'] = time.time()
        Timing.times['tracking_time'] = Timing.times['sec_time']
        Trials.trial_running = True

        #if GUI.connectTo.get() == 'Blender':
        """
        if maze.waypoint == []:  # empty(first iteration)
            startCoords = GUI.Canvas.coords(maze.startPosMarker)
            maze.waypoint.append((startCoords[0] + 5, startCoords[1] + 5))
            maze.waypoint.append((
                var.mouse_x + float(GUI.get('maze_grid', 'xpos')),
                var.mouse_y + float(GUI.get('maze_grid', 'ypos'))
            ))

            data[0].add_row(
                time=Timing.get_time(), xpos=var.mouse_x, ypos=var.mouse_y)

            maze.tracklines.append(
                GUI.Canvas.create_line(
                    maze.waypoint[-2][0], maze.waypoint[-2][1],
                    maze.waypoint[-1][0], maze.waypoint[-1][1],
                    fill="#66CD00", dash=2))
        """
    
    else:
        Timing.times['sec'] = 0
        Timing.times['sec_time'] = time.time()
        Timing.times['tracking_time'] = Timing.times['sec_time']


def calc_move(GUI, maze, trial_data):
    """
    calculate movement length and save data
    """
    trial_data.add_row(time=Timing.get_time(), xpos=var.mouse_x, ypos=var.mouse_y)
    #Timing.times['tracking_time'] = time.time()

    # sufficient movement?
    if maze.waypoint != []:
        lenght = (
            ( (var.mouse_x + float(GUI.get('maze_grid', 'xpos'))) 
            - (maze.waypoint[-1][0]) ) ** 2 
            + ( (var.mouse_y + float(GUI.get('maze_grid', 'ypos'))) 
            - maze.waypoint[-1][1] ) ** 2) ** 0.5
    else:
        lenght = 1
    # critical lenght
    if lenght >= 1 and len(maze.waypoint) > 0:
        maze.waypoint.append(
            (var.mouse_x + float(GUI.get('maze_grid', 'xpos')),
            var.mouse_y + float(GUI.get('maze_grid', 'ypos'))))
        maze.tracklines.append(
            GUI.Canvas.create_line(
                maze.waypoint[-2][0], maze.waypoint[-2][1],
                maze.waypoint[-1][0], maze.waypoint[-1][1], 
                fill="#66CD00", dash=2))
