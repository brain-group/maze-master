import time
import datetime

times = {
    'tracking_time': time.time(), #TrackingTime
    'wait_for_trigger': False, #WaitForTrigger
    'TM_end_time': 0, #TMEndTime
    'TM_end_timeak_time': 0, #TMEndTimeakTime TMEndTimeakTime
    'sec': time.time(), #sec
    't_start': 0.0, #t_start
    'sec_time': time.time(), #SecTime 
    'act_sec': -1, #exp.actSec
    'session_time_start': time.time(), 
    'session_time': 0.0,
    'sensor_delay': 0,
    'reward_delay':0
    }

def timeStrTo_msecs(timestr):
    _split = timestr.split(':')
    return int(_split[0])*360000 + int(_split[1])*60000 + float(_split[2])*1000

def get_time(time_in_sec=None):
    if time_in_sec is None:
        return datetime.datetime.now().strftime("%H:%M:%S.%f")[:-3]
    else:
        s, ms = divmod(time_in_sec * 1000, 1000)
        return '{}.{:03d}'.format(
            time.strftime('%H:%M:%S', time.gmtime(s)), round(ms))
