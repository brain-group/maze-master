# -*- coding: utf-8 -*-
"""
Created on Wed Mar 14 11:59:22 2018

@author: bexter
"""
import time
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox as mbox
from GUI_scripts import GUIScripts
import random
import Timing
import os
import numpy as np
import csv
import tarfile
from PIL import Image, ImageOps
from scipy import misc      
import var

class Stimulus:
    """
    class for all kind of stimuli,
    containing visual cues, rewards and air puffs
    """
    
    def __init__(self, config):

        if var.NI:
            import nidaqmx
            
            # Air Puff manual
            self.DOAirPuff = nidaqmx.Task()
            self.DOAirPuff.do_channels.add_do_chan(
                config.get('NIlines', 'air puff'), 
                name_to_assign_to_lines='AirPuff')
            
            # Air Puff left
            self.DOAirPuffL = nidaqmx.Task()
            self.DOAirPuffL.do_channels.add_do_chan(
                config.get('NIlines', 'air puff left'), 
                name_to_assign_to_lines='AirPuffL')
            
            # Air Puff right
            self.DOAirPuffR = nidaqmx.Task()
            self.DOAirPuffR.do_channels.add_do_chan(
                config.get('NIlines', 'air puff right'), 
                name_to_assign_to_lines='AirPuffR')

            # answer signal in T-maze
            self.DOtaskR = nidaqmx.Task()
            self.DOtaskR.do_channels.add_do_chan(
                config.get('NIlines', 'reward r'), 
                name_to_assign_to_lines='rewardR')

            # answer signal in T-maze
            self.DOtaskL = nidaqmx.Task()
            self.DOtaskL.do_channels.add_do_chan(
                config.get('NIlines', 'reward l'), 
                name_to_assign_to_lines='rewardL')
        #

        self.flush = False
        self.airPuff_time = []
        for i in range(3):            
            self.airPuff_time.append(0.0)
        self.rewardOpenR_time = 0.0
        self.rewardOpenL_time = 0.0
        self.noCuesTotal = 10
        self.flashRule = 0  # number of rule which is created to teleport
        self.sequWindow = None
        self.cueSet = None

        # self.GUI = GUI

        self.stimfile = None
        self.mmconfig = config
        self.config_section_name = 'Stimulus'
        self.target_list_given = False
        self.target_list_visual = []
        self.distractor_list_visual = []

        self.modality_wise_given = False

        self.target_list_tactile = []
        self.distractor_list_tactile = []

    #
   
    def set_cue(self, server, trial_data, duration, cueNoL=None, cueNoR=None):
        """
        Place the given cues at the wall of the maze, either right, left or
        both sides for a given time period (duration).
        """ 
        
        #save a data line
        trial_data.add(
            time= Timing.get_time(), xpos=var.mouse_x, ypos=var.mouse_y,
            cue_right=cueNoR, cue_left=cueNoL)
        
        if cueNoL == cueNoR and cueNoL != None: #both
            server.handler.handle_send("$both#%s*%s" % (cueNoL, duration))
        else:    
            if cueNoL != None and cueNoL != -1:
                server.handler.handle_send("$left#%s*%s" % (cueNoL, duration))
            if cueNoR != None and cueNoR != -1:    
                server.handler.handle_send("$right#%s*%s" % (cueNoR, duration))

    def apply_air_puff(self, GUI, trial_data, side=None): 
        if var.NI:
            """Start Airpuff and set closing timepoint"""
            if side==None:
                self.DOAirPuff.write(True, auto_start=True)
                self.DOAirPuff.stop()
                self.airPuff_time[0] = time.time() + \
                    float(GUI.ePuffDuration.get())
                
            elif side == 'left':
                self.DOAirPuffL.write(True, auto_start=True)
                self.DOAirPuffL.stop()
                self.airPuff_time[1] = time.time() + \
                    float(GUI.ePuffDuration.get())
                # save a data line
                trial_data.add_row(
                        time=Timing.get_time(), xpos=var.mouse_x,
                        ypos=var.mouse_y,
                        cue_left='Airpuff ' + GUI.ePuffDuration.get(), 
                        cue_right='-1')  
                
            elif side == 'right':
                self.DOAirPuffR.write(True, auto_start=True)
                self.DOAirPuffR.stop()
                self.airPuff_time[2] = time.time() + \
                    float(GUI.ePuffDuration.get())
                # save a data line
                trial_data.add_row(
                        time=Timing.get_time(), xpos=var.mouse_x,
                        ypos=var.mouse_y,
                        cue_right='Airpuff ' + GUI.ePuffDuration.get(), 
                        cue_left='-1')   
                
    def airpuff_update(self):
        """Check if closing timepoint is reached and close the valve"""
        if time.time() > self.airPuff_time[0] and self.airPuff_time[0] != 0.0:
            self.DOAirPuff.write(False, auto_start=True)
            self.DOAirPuff.stop()
            self.airPuff_time[0] = 0.0
            
        if time.time() > self.airPuff_time[1] and self.airPuff_time[1] != 0.0:
            self.DOAirPuffL.write(False, auto_start=True)
            self.DOAirPuffL.stop()
            self.airPuff_time[1] = 0.0   
            
        if time.time() > self.airPuff_time[2] and self.airPuff_time[2] != 0.0:
            self.DOAirPuffR.write(False, auto_start=True)
            self.DOAirPuffR.stop()
            self.airPuff_time[2] = 0.0   
            
    def flushSpouts(self, GUI, event=None):
        """Open Both reward valves if they are clsoed
        or close them if there are open"""
        if self.flush:
            self.flush = False
            GUI.flushButton.config(bg='#ffffff')
            if var.digital_output:
                self.DOtaskL.write(False, auto_start=True)
                self.DOtaskL.stop()
                self.DOtaskR.write(False, auto_start=True)
                self.DOtaskR.stop()
        else:
            self.flush = True
            GUI.flushButton.config(bg="green")
            if var.digital_output:
                self.DOtaskL.write(True, auto_start=True)
                self.DOtaskL.stop()
                self.DOtaskR.write(True, auto_start=True)
                self.DOtaskR.stop()

    def setTeleportRule(self, endless, rules, GUI, GUIrules, maze):
        """Open rules window and set teleport rule"""
        
        if endless: #todo
            if not GUI.isOpen(GUIrules):
                  GUIrules.openWindow(maze, rules)         
              
            _ruleDict = {"sensor_var1":0,"teleport":True}
            rules.define_Rule(0, _ruleDict, GUIrules)

    def loadFlash(self, endless, rules, GUI, GUIrules, maze):
        """Creates a counterpart for each cue, which i for the other side of 
        the wall(flipped). Open the Rules window and create rules for flashing
        all cues at the corresponding sensor in ascending order"""
        import os
        if endless: #todo
          if not GUI.isOpen(GUIrules):
              GUIrules.openWindow(maze, rules)
          
          
          _ruleDict = {"sensor_var1":0,"teleport":True}
          rules.define_Rule(0, _ruleDict, GUIrules)

          pathname = "Data/wall_textures/"
          for n in range(self.noCuesTotal):
              if os.path.isfile(pathname + "cues/cue_"+str(n)+".png"):

                  with open(pathname + "cues/cue_"+str(n)+".png", 'rb') as \
                  image_file:
                      with Image.open(image_file) as image:

                          #create mirror image for the other side of the wall
                          #image_mirrored = ImageOps.mirror(image)
                          image_mirrored = ImageOps.flip(image)
                          image_mirrored.save(
                                  pathname + "cues/cue_"+str(n)+"_mirr.png")
                          
                          #Set Rule
                          GUIrules.add_rule_row(rules)
                          _ruleDict = {"sensor_var1":n+1,"teleport":False,
                                       "rule_cue_direction":"both",
                                       "cuesVar1":n, "cuesVar2":n}
                          rules.define_Rule(-1, _ruleDict, GUIrules)
                          
              else:
                  #clear texture slot
                  GUI.CueTexturePhoto[n] = None
                  
                  #Set Rule
                  GUIrules.add_rule_row(rules)
                  _ruleDict = {"sensor_var1":n+1,"teleport":False,
                               "rule_cue_direction":"both",
                               "cuesVar1":-1, "cuesVar2":-1}
                  rules.define_Rule(-1, _ruleDict, GUIrules)
                          
                          
#    def createRandomSequ(self, no_trials, target_multi = 1):
#        stimArray=[]
#        for file in os.listdir("../Data/wall_textures/target"):
#            if file.endswith(".png") or file.endswith(".jpg"):
#                for n in range(target_multi):
#                    stimArray.append(file[:-4])
#        for file in os.listdir("../Data/wall_textures/random"):
#            if file.endswith(".png") or file.endswith(".jpg"):
#                stimArray.append(file[:-4])
#                
#        no_trials = no_trials+5 #buffer at the end
#        
#        while len(stimArray)< no_trials:
#            stimArray = stimArray+stimArray
#                
#        random.shuffle(stimArray)
#        self.createImages(stimArray[:no_trials])  
#        return stimArray[:no_trials]
    
    def createWallTexture(self, config):  
        """creates a wall texture file from all cues in the cue folder and 
        saves it in the wall_textures folder"""
        
        WallImages = []
        for file in os.listdir("Data/wall_textures/cues"):
          if file.endswith(".png") and not file.endswith("mirr.png"):
              WallImages.append("Data/wall_textures/cues/"+file)
     
        config.set_value('conf', 'multText','False')
        
        if len(WallImages) > 0:
            imgs = [ Image.open(i) for i in WallImages ]

            # pick the image which is the smallest, and resize the 
            #others to match it (can be arbitrary image shape here)
            
            #min_shape = sorted([(np.sum(i.size), i.size) for i in imgs])[0][1]

            #imgs_comb = np.hstack((
            #    np.asarray( i.resize(min_shape) ) for i in imgs))
            
            #imgs_comb = np.hstack((
            #    np.asarray(i) for i in imgs))
            
            
            widths, heights = zip(*(i.size for i in imgs))

            total_width = sum(widths)
            max_height = max(heights)
            
            imgs_comb = Image.new('RGB', (total_width, max_height))
            
            
            x_offset = 0
            for im in imgs:
              imgs_comb.paste(im, (x_offset,0))
              x_offset += im.size[0]
            
            #new_im.save('test.jpg')


            # save that beautiful picture
            #imgs_comb = Image.fromarray( imgs_comb)
            #mirror it
            imgs_comb = ImageOps.mirror(imgs_comb)
            
            
            
            if os.path.isdir("Data/wall_textures/maze_textures/_temp"):
                imgs_comb.save(
                    "Data/wall_textures/maze_textures/_temp/wallTexture.png")                 
            imgs_comb.save( "Data/wall_textures/wallTexture.png" )

            width, height = imgs_comb.size
            ratioTex = str(int(width/height))
        
            config.set_value('conf', 'imgfactl', ratioTex)
    #

    def createSequenceImage(self, trial, maze, config, saveAs = None): 
      """creates a wall texture image for a trial for the endless corridor
      based on the cue numbers in a stimulus file""" 

      WallImages = []
      
      no_sensors = int(maze.data.get('settings', 'endless_length')) 
      
      endlessFactor = int(maze.data.get('settings', 'endless_factor'))

      for i in range(endlessFactor):
          order = self.distractor_list_visual[int((trial-1+i)*no_sensors):
                      int((trial-1+i)*no_sensors + no_sensors)]
              
          while len(order) < no_sensors:
              order.append(self.distractor_list_visual[0])
              
          order = [int(i) for i in order]
          WallImages.append(self.createEndlessWallTexture(order = order))   

      imgs_comb = np.hstack(np.asarray(WallImages))

      # save that beautiful picture
      imgs_comb = Image.fromarray( imgs_comb)

      #mirror the image
      imgs_comb = ImageOps.mirror(imgs_comb)
      if saveAs == None:
          imgs_comb.save("Data/wall_textures/wallTexture.png")  
      else:
          imgs_comb.save(saveAs)
          
      #save walltexture for 1st trial    
      if trial==1:
         imgs_comb.save("Data/wall_textures/wallTexture.png")   
          
      
      width, height = imgs_comb.size
      ratioTex = str(int(width/height))
        
      config.set_value('conf', 'imgfactl', ratioTex)
    #
      
    def ReadLengfhtofMaze(self, maze):
        """return length of tunnel maze  wall as a factor length/heigth"""
        import ast
        return (float(ast.literal_eval(maze.data.get('Walls', '0'))[3]) - \
                float(ast.literal_eval(maze.data.get('Walls', '0'))[1]))/20.0
    #

    def createAllSequence(self, maze, config):
        """create all sequence images for all trials based on a given order 
        from a stimulus file"""

        if maze.data.get('settings', 'endless') == 'True': 
            no_sensors = int(maze.data.get('settings', 'endless_length'))
            
            for i in range(1,int(len(self.distractor_list_visual)/no_sensors)+1):
                self.createSequenceImage(i, maze, config, 
                            saveAs ="Data/wall_textures/maze_textures/_temp/"\
                            "sequences/wallTexture" + str(i) + ".png" )
        else:
                    
            config.set_value('conf', 'imgfactl', 
                             str(int(self.ReadLengfhtofMaze(maze))))
  
        config.set_value('conf', 'multText', 'False')
    #
        
    def activate_false_wall(self, No, server):
        server.handler.handle_send("[" + str(No) + "#")
        print('FalseWall', No)
        
    def reward_update(self, config, tracking_device, data, GUI, maze, server, 
                      TTL, trial, rules, GUI_Behavior, GUI_Rules, GUI_FT):
        """Check if reward time is over, closes the valves and starts a new 
        trial if necessary (if newTrialReward in config is True)"""
        
        if 0 < self.rewardOpenR_time <= Timing.times['session_time']:
            self.rewardOpenR_time = 0
            if var.NI:
                self.DOtaskR.write(False, auto_start=True)
                self.DOtaskR.stop()
            if bool(int(config.get('conf', 'newTrialReward'))):
                if not GUI_Behavior.isOpen() or GUI.autoReward.get():
                    trial.new_trial(
                        config, tracking_device, data, rules, GUI_Rules, GUI, 
                        GUI_Behavior, maze, server, TTL, self, GUI_FT)

        if 0 < self.rewardOpenL_time <= Timing.times['session_time']:
            self.rewardOpenL_time = 0
            if var.NI:
                self.DOtaskL.write(False, auto_start=True)
                self.DOtaskL.stop()
            if bool(int(config.get('conf', 'newTrialReward'))):
                if not GUI_Behavior.isOpen() or GUI.autoReward.get():
                    trial.new_trial(
                        config, tracking_device, data, rules, GUI_Rules, GUI, 
                        GUI_Behavior, maze, server, TTL, self, GUI_FT)
    #
            
    def close(self):
        """
        Close and delete all tasks
        """
        self.DOAirPuff.stop()
        self.DOAirPuff.close()
        del self.DOAirPuff
        self.DOtaskR.stop()
        self.DOtaskR.close()
        del self.DOtaskR
        self.DOtaskL.stop()
        self.DOtaskL.close()
        del self.DOtaskL

    def give_reward(self, trial_data, duration, probability, side):
        """
        decide if reward is going to be given (probability is given),
        then open the valve on the given side. Set closing time and save
        event in data file.
        """
        
        if random.randint(0, 100) <= probability:
            
            #check for script
            for scripts in GUIScripts.scripts:
                if scripts['trigger'].get()=='On Reward':
                    GUIScripts.executeScript(scripts)
            
            trial_data.add_row(
                time=Timing.get_time(), xpos=var.mouse_x, ypos=var.mouse_y,
                reward=side + ' ' + str(duration))

            if side == 'L' or side == 'left':
                if var.NI:
                    self.DOtaskL.write(True, auto_start=True)
                    self.DOtaskL.stop()
                self.rewardOpenL_time = Timing.times['session_time'] + duration

            elif side == 'R' or 'right' or side == None:
                if var.NI:
                    self.DOtaskR.write(True, auto_start=True)
                    self.DOtaskR.stop()
                self.rewardOpenR_time = Timing.times['session_time'] + duration
            else:
                print("invalid side definition for reward")
        else:
            trial_data.add_row(
                time=Timing.get_time(), xpos=var.mouse_x, ypos=var.mouse_y,
                reward="omitted " + str(probability))
            print("Reward omitted")

    def loadCue(self, cueNo):
        """Load a cue from a grafic file and return it"""
        
        import shutil
        from PIL import Image, ImageTk
    
        CueTex = filedialog.askopenfilename()
        if CueTex != '':
            shutil.copyfile(CueTex, "Texture_"+str(cueNo)+".bmp")     
            with open("Texture_"+str(cueNo)+".bmp", 'rb') as image_file:
                with Image.open(image_file) as image:
                    image.thumbnail((34, 34), Image.ANTIALIAS)
                    return ImageTk.PhotoImage(image)
    #

    def checkCuesMatch(self, maze):
        """checks if the length of the endless corridor matches the number
        of cues loaded"""
        
        #no of sensors/endless length    
        if maze.data.get('settings', 'endless') == 'True': 
            WallImages = []
            for file in os.listdir("Data/wall_textures/cues"):
              if file.endswith(".png") and not file.endswith("mirr.png"):
                  WallImages.append("Data/wall_textures/cues/"+file)
 
            if len(WallImages) != int(maze.data.get('settings', 
                                                    'endless_length')):
                
                
                if mbox.askokcancel("Cue Number not matching", 
                                    "The number of cues does not match the"\
                                    "length of the maze. Continue?"):
                    return True
                else:
                    return False
            else:
                return True
        else:
            return True
    #

    def loadStimList(self, load_from_configs=False):
        """Opens a file dialog for loading a list of stimuli file and returns 
        the content as a list of stimuli IDs"""
        
        if (not load_from_configs) or (self.stimfile is None) or self.stimfile == '':
            self.stimfile = filedialog.askopenfilename(multiple=False,
                                                       title="Choose a data file",
                                                       defaultextension='.csv',
                                                       filetypes=[('csv file', '*.csv')])
        #
    
        with open(self.stimfile) as csvfile:
            reader = csv.DictReader(csvfile, dialect='excel',
                                    quotechar='"', quoting=csv.QUOTE_ALL,
                                    lineterminator='\n', delimiter=',')

            # header_row = True
            header = reader.fieldnames

            # I'm distinguishing 2 mode:
            #     1. just distractors given -> modality_wise_given == False,
            #     3. targets &  distractors given, for both modalities individually -> modality_wise_given == True
            self.modality_wise_given = ('target_visual' in header) and ('distractor_visual' in header) and ('target_visual' in header) and ('distractor_visual' in header)
            if self.modality_wise_given:
                self.target_list_visual = []
                self.distractor_list_visual = []
                self.target_list_tactile = []
                self.distractor_list_tactile = []
            #

            # Now load from the files
            if self.modality_wise_given:
                for row in reader:
                    self.target_list_visual.append(row['target_visual'])
                    self.distractor_list_visual.append(row['distractor_visual'])
                    self.target_list_tactile.append(row['target_tactile'])
                    self.distractor_list_tactile.append(row['distractor_tactile'])
                #
            else:
                # the current default:
                if 'cue' in header:
                    for row in reader:
                        self.distractor_list_visual.append(row['cue'])
                    #
                elif 'distractor' in header:
                    for row in reader:
                        self.distractor_list_visual.append(row['distractor'])
                    #
            #

            csvfile.close()
        #
    #

    def save_config(self):
        if self.config_section_name not in self.mmconfig.winConfig.sections():
            self.mmconfig.winConfig.add_section(self.config_section_name)
        #

        self.mmconfig.set_value(self.config_section_name, 'Stimulus_file', self.stimfile)
    #

    def load_config(self):
        successfully_loaded = False
        try:
            if self.config_section_name in self.mmconfig.winConfig.sections():  # Only try to load if the section exists
                self.stimfile = self.mmconfig.get(self.config_section_name, 'Stimulus_file')
                successfully_loaded = True
            #
        except Exception as e:
            print(e)
            print('Unable to load last stimulus-file!')
            self.mmconfig.winConfig.remove_section(self.config_section_name)
        #

        return successfully_loaded
    #

    def openStimListWindow(self, GUI, load_from_configs=False):
        """Creates a window for displaying the list of Stimuli. The list is 
        filled with the stimulus IDs using the loadStimList function"""
        self.masterSequ = tk.Tk()
        self.masterSequ.title("Stimulus")
        self.tStimList = tk.Text(
            self.masterSequ, insertofftime=0, relief=tk.SUNKEN, bg="white",
            wrap=tk.WORD)
    
        self.tStimList.grid(row=0,column=0)
        self.tStimList.config(state=tk.NORMAL)
        if load_from_configs:
            successfully_loaded = self.load_config()
            if not successfully_loaded:
                return successfully_loaded
        #

        self.loadStimList(load_from_configs)
        for i, value in enumerate(self.distractor_list_visual):
            if self.modality_wise_given:
                self.tStimList.insert(tk.END, self.target_list_visual[i] + '|' + self.target_list_tactile[i] + ' - '
                                      + self.distractor_list_visual[i] + '|' + self.distractor_list_tactile[i] + '\n')
            else:
                self.tStimList.insert(tk.END, value + '\n')
            #
            # self.tStimList.insert(tk.END, i + '\n')
        #
        filename = "Seq:  " + str(os.path.basename(self.stimfile))
        GUI.sequFileName.config(text=filename)

        self.save_config()
    #

    def updateStimList(self, trial, no_sensors):
        """updates stimulus list with yellow highlighting"""
        self.tStimList.tag_delete('bg')
        # TODO: should be optimized, not simple number of sensors   
        self.tStimList.tag_add('bg',float((trial-1) * no_sensors)+1,
                               float((trial-1) * no_sensors + no_sensors+1))
        self.tStimList.tag_config("bg", background="yellow")    
    #
    
    def updateMultipleTextures(self, trial, config, targetSide, current_target_visual,
                               modality):
        """creates 2 texture files for the walls in a non-endless corridor for
        a 2-afc behavior task"""  
    
        wallImages_target = []
        wallImages_dist = []
        noCues = int(len(current_target_visual))

        # order is the distractor order
        # current_target_visual is given as argument by default
        current_distractor_visual = self.distractor_list_visual[int((trial-1)*noCues):int((trial-1)*noCues + noCues)]
        if self.modality_wise_given:
            current_distractor_tactile = self.distractor_list_tactile[int((trial-1)*noCues):int((trial-1)*noCues + noCues)]

            # if available, overwrite the default current_target_visual
            # for some reason the targets have to be reversed?!
            current_target_visual = self.target_list_visual[int((trial-1)*noCues):int((trial-1)*noCues + noCues)]
            current_target_visual.reverse()
            current_target_tactile = self.target_list_tactile[int((trial-1)*noCues):int((trial-1)*noCues + noCues)]
            current_target_tactile.reverse()
        else:
            current_target_tactile = current_target_visual
            current_distractor_tactile = current_distractor_visual
        #

        while len(current_distractor_visual) < noCues:
            current_distractor_visual.append(self.distractor_list_visual[0])

        current_distractor_visual = [int(i) for i in current_distractor_visual]
        current_target_visual = [int(i) for i in current_target_visual]
        
        if modality == 0:
            # only visual
            current_distractor_tactile = [0 for i in current_distractor_tactile]
            current_target_tactile = [0 for i in current_target_tactile]
        elif modality == 1:
            # only tactile
            current_distractor_visual = [0 for i in current_distractor_visual]
            current_target_visual = [0 for i in current_target_visual]
        #

        # create wall textures from current_distractor_visual & current_target_visual
        wallImages_dist.append(self.createEndlessWallTexture(order=current_distractor_visual))
        wallImages_target.append(self.createEndlessWallTexture(order=current_target_visual))

        #
        imgs_comb_dist = np.hstack(np.asarray(wallImages_dist))
        imgs_comb_target = np.hstack(np.asarray(wallImages_target))
        
        # save that beautiful picture
        imgs_comb_dist = Image.fromarray(imgs_comb_dist)
        imgs_comb_target = Image.fromarray(imgs_comb_target)
        
        if targetSide == 'right':
            imgs_comb_target.save(
                "Data/wall_textures/maze_textures/_temp/wallTextureR.png")
            
            # mirror it
            imgs_comb_dist = ImageOps.mirror(imgs_comb_dist)
            imgs_comb_dist.save(
                "Data/wall_textures/maze_textures/_temp/wallTextureL.png")                 
        elif targetSide == 'left':
            imgs_comb_target.save(
                "Data/wall_textures/maze_textures/_temp/wallTextureL.png")  

            # mirror it
            imgs_comb_dist = ImageOps.mirror(imgs_comb_dist)          
            imgs_comb_dist.save(
                "Data/wall_textures/maze_textures/_temp/wallTextureR.png")
        #
        
        config.set_value('conf', 'multText', 'True')

        return current_target_visual, current_distractor_visual, current_target_tactile, current_distractor_tactile
    #

    def isOpen(self):
        """Check if stimulus list window is open and return state as bool"""
        try:
            if 'normal' == self.masterSequ.state():
                return True
            else:
                return False
        except:
            return False                
    #

    def saveCueSet(self):
        """Saves all loaded cues as a stimulus set (.tar) at a given location 
        (via filedialog)"""
        
        f = filedialog.asksaveasfilename(
                initialdir="Data/wall_textures/cues/cueSets/",
                title='select a filename to save',
                defaultextension=".tar")
        
        out = tarfile.open(f, mode='w')
        for file in os.listdir("Data/wall_textures/cues"):
            if file.endswith(".png") and not file.endswith("mirr.png") :
                out.add("Data/wall_textures/cues/" + file, 
                        arcname=os.path.basename(file))
        out.close()
    #
        
    def loadCueSet(self, GUI): 
        """Loads a set of stimuli (.tar) via a filedialog and replaces all cues
        with the loaded set"""
        
        f = filedialog.askopenfilename(
                initialdir="Data/wall_textures/cues/cueSets/",
                title='Pick a file to load',
                defaultextension=".tar")
        
        #delete all files in cue folder (all old cues)
        for file in os.listdir("Data/wall_textures/cues/"):
            file_path = os.path.join("Data/wall_textures/cues/", file)
            if os.path.isfile(file_path) and file.endswith(".png"):
                os.unlink(file_path)
                    
        t = tarfile.open(f, 'r')
        t.extractall("Data/wall_textures/cues/")
        
        for i in range(self.noCuesTotal):
            # search for existing textures:
            GUI.loadCue(i, GUI.frames[i])
    #
        
    def createEndlessWallTexture(self, order = None):
        """Creates an image file from all cues in the ascending order 
        and returns it"""
        WallImages = []
     
        for file in os.listdir("Data/wall_textures/cues"):
          if file.endswith(".png") and not file.endswith("mirr.png"):
              WallImages.append("Data/wall_textures/cues/"+file)
              
        imgs = [ Image.open(i) for i in WallImages ]
        imgs = [i.convert('RGB') for i in imgs]
 
        #rearrange 
        if order != None:
            imgs = [imgs[i] for i in order]
    
        imgs_comb = np.hstack((np.asarray(i) for i in imgs))

        return imgs_comb
#
