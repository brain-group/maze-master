# -*- coding: utf-8 -*-

#!/usr/bin/python
import socket        
import asyncore
import Timing

from MM_tracking import Tracking
from MM_data_container import DataContainer

class ServerHandler(asyncore.dispatcher_with_send):
    #global ReadData
    #def __init__(self):
    #    self.readData = ''

    def handle_read(self):
        #global ReadData
        data = self.recv(1024)
        if data:            
            self.readData = data.decode("utf-8")

    def handle_send(self, msg):
        OutSend = bytes((msg), 'UTF-8')
        self.send(OutSend)         


class Server(asyncore.dispatcher): 
    
    def __init__(self, host, port):
        self.running = False
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind((host, port))
        
    def handle_accept(self):
        """Accepts the connection to the client"""
        pair = self.accept()
        if pair is not None:
            sock, addr = pair
            print ('Incoming connection from %s' % repr(addr))
            self.handler = ServerHandler(sock)
            self.handler.readData = ''
            self.running = True
            self.onlineLabel.config(text="Online", foreground="GREEN")
            
    def handle_close(self):
        self.close()            

    def start_server(self, GUI, meta_data, autostartBlender, stim, config, 
                     maze, rules, GUIrules):
        """Starts a server which waits for a connection. If autostartBlender 
        is True, Blender is started automatically and will connect"""
        import time
        
        if not self.running:
            #initialize stimuli
            if not stim.checkCuesMatch(maze):
                return
            if GUI.cuesBackground.get() and stim.isOpen():
                stim.createAllSequence(maze, config)
            else:
                stim.createWallTexture(config)

            if not GUI.cuesBackground.get() and stim.isOpen():
                config.set_value('conf', 'multText','True')
                
                config.set_value('conf', 'imgfactl', 
                             str(int(stim.ReadLengfhtofMaze(maze))))
            
            #flash cues
            if GUI.flashCues.get():
                stim.loadFlash(maze.data.get('settings', 'endless') == 'True',
                                   rules, GUI, GUIrules, maze)
            else:
                stim.setTeleportRule(maze.data.get('settings', 'endless') == 'True',
                                   rules, GUI, GUIrules, maze)
    
            
            import os
            os.startfile('createMazeBlender.bat')
            time.sleep(8)
            # connect to selected server engine
            if GUI.connectTo.get() == 'Blender':
                print('getBlender')
            self.listen(5)
            self.onlineLabel = GUI.LOnline
            # update label color to new status
            self.onlineLabel.config(text="Searching...", foreground='ORANGE')
            # check that no existing filename is overwritten 
            meta_data.check_against_overwrite(
                GUI, int(GUI.eBlockID.get())+1, 
                GUI._get_file_basename())
            # save header block of metadata
            meta_data.data_to_csv(
                GUI.saveFilesTo.get(), 
                GUI.eDataDir.get(), 
                GUI.eFileBaseName.get())
            DataContainer.save_temp_config(GUI)
            
            # block metadata: update data and file
            if not GUI.saveFilesTo.get() == "Nowhere":
                meta_data.update(StartTime = Timing.get_time(), 
                                 MouseID = GUI.eMouseID.get())
                meta_data.replace_data_in_file(GUI.saveFilesTo.get(), 
                                               GUI.eDataDir.get(),
                                               GUI.eFileBaseName.get(),
                                               StartTime = [Timing.get_time()],
                                               MouseID = [GUI.eMouseID.get()])
            
            #Start blender when autostart is enabled
            if autostartBlender:
                import os
                os.startfile('startBlender.bat')
        else:
            print("Server already running")


    def stopServer(self, data, GUI, trial):
        """Stops the running experiment and closes the connection to blender"""
        if self.running:
            self.handler.handle_send('{')
        self.handle_close()
        print("Server closed")
        self.running = False
        if GUI.Canvas.winfo_exists():
            GUI.LOnline.config(text="Offline", foreground='RED')
            data[0].save_data(
                data, GUI.saveFilesTo.get(), GUI.eDataDir.get(),
                GUI.eFileBaseName.get(), GUI.timeDisplay, Timing.get_time(), 
                GUI)

        Tracking.tracking =  False
        trial.trial_running = False
