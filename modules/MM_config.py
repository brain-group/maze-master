# -*- coding: utf-8 -*-
"""
Created on Thu Mar  8 16:27:44 2018

@author: bexter
"""

import configparser

class Config:
    
    def __init__(self, filename):
        self.winConfig = configparser.ConfigParser()
        self.winConfig.optionxform=str
        self.winConfig.read(filename)
        self.configFile = filename

    def get(self, window_name, field_name):        
        return self.winConfig.get(window_name,field_name)
    
    def set_value(self, group_name, field_name, value):
        self.winConfig[group_name][field_name] = value
        with open(self.configFile, 'w+') as configfile:
            self.winConfig.write(configfile) 
            
    def add_section(self, sectionName):
        self.winConfig.add_section(sectionName)
        with open(self.configFile, 'w+') as configfile:
            self.winConfig.write(configfile) 
            
    def delete_section(self, sectionName):
        self.winConfig.remove_section(sectionName)
        with open(self.configFile, 'w+') as configfile:
            self.winConfig.write(configfile)         

    def delete_value(self, group_name, field_name):
        self.winConfig.remove_option(group_name, field_name)
        with open(self.configFile, 'w+') as configfile:
            self.winConfig.write(configfile)  
            
           
            