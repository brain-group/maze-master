

class Rules:
    
    def __init__(self):
        self.rules = []
        self.teleport_active = True

    def add_rule(self, ruleDict):
        self.rules.append(ruleDict)

    def define_Rule(self, Rule_no, ruleDict, GUIrules):
        for key in ruleDict: 
            self.rules[Rule_no][key] = ruleDict[key]
            
        GUIrules.updateRules(self.rules)
